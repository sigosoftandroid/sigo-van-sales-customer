package com.vansale.customer.Model;

public class OrderDetailPdtModel {
    String product_id;
    String name_latin;
    String name_arabic;
    String total;
    String quantity;
    String image;

    public String getName_arabic() {
        return name_arabic;
    }

    public void setName_arabic(String name_arabic) {
        this.name_arabic = name_arabic;
    }



    public OrderDetailPdtModel(String product_id,String name_latin, String name_arabic, String quantity, String total, String image) {

        this.product_id = product_id;
        this.name_arabic = name_arabic;
        this.quantity = quantity;
        this.total = total;
        this.name_latin = name_latin;
        this.image = image;
    }


    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getName_latin() {
        return name_latin;
    }

    public void setName_latin(String name_latin) {
        this.name_latin = name_latin;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


}
