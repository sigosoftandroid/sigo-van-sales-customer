package com.vansale.customer.Model;

public class MyOrdersModel {
    String date;
    String order_id;
    String delivery_status;
    String timestamp;

    public MyOrdersModel(String order_id, String delivery_date, String delivery_status,String timestamp) {

        this.order_id = order_id;
        this.date = delivery_date;
        this.delivery_status = delivery_status;
        this.timestamp = timestamp;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRequest_id() {
        return order_id;
    }

    public void setRequest_id(String request_id) {
        this.order_id = request_id;
    }

    public String getDelivery_status() {
        return delivery_status;
    }

    public void setDelivery_status(String delivery_status) {
        this.delivery_status = delivery_status;
    }
    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
