package com.vansale.customer.Model;

public class ProductModel {

    String cart_id;
    String subtotal;
    String product_id;
    String category_id;
    String product_name;
    String product_name_arabic;
    String product_vat;
    String category;
    String price;
    String image;
    String quantity;
    String stock;
    String timestamp;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    String coupon;

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }



    // constructor for view cart products
    public ProductModel(String product_id, String product_name_english, String product_name_arabic, String cat_id, String price, String product_vat, String image, String quantity, String cart_id, String stock, String subtotal) {
        this.product_id =product_id;
        this.product_name =product_name_english;
        this.product_name_arabic = product_name_arabic;
        this.category_id = cat_id;
        this.price = price;
        this.product_vat = product_vat;
        this.image = image;
        this.quantity = quantity;
        this.cart_id= cart_id;
        this.stock = stock;
        this.subtotal = subtotal;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_name_arabic() {
        return product_name_arabic;
    }

    public void setProduct_name_arabic(String product_name_arabic) {
        this.product_name_arabic = product_name_arabic;
    }

    public String getProduct_vat() {
        return product_vat;
    }

    public void setProduct_vat(String product_vat) {
        this.product_vat = product_vat;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }



    // constructor for view products
    public ProductModel(String product_id, String category_id, String product_name, String product_name_arabic, String product_vat, String price, String image, String category, String quantity,String stock,String coupons, String timestamp ) {
      this.product_id = product_id;
      this.category_id = category_id;
      this.product_name = product_name;
      this.product_name_arabic = product_name_arabic;
      this.product_vat = product_vat;
      this.price =price;
      this.image=image;
      this.category= category;
      this.quantity = quantity;
      this.stock = stock;
      this.coupon = coupons;
      this.timestamp = timestamp;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
