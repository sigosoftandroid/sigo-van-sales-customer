package com.vansale.customer.Model;

public class CouponsModel {
    String total_coupons;
    String no_of_used_bottles;
    String no_of_unused_bottles;
    String expiry;
    String coupon_name;
    String coupon_name_arabic;
    String pack_price;
    String pack_validity;
    String product_name;
    String product_name_arabic;

    public String getTotal_coupons() {
        return total_coupons;
    }

    public void setTotal_coupons(String total_coupons) {
        this.total_coupons = total_coupons;
    }

    public String getNo_of_used_bottles() {
        return no_of_used_bottles;
    }

    public void setNo_of_used_bottles(String no_of_used_bottles) {
        this.no_of_used_bottles = no_of_used_bottles;
    }

    public String getNo_of_unused_bottles() {
        return no_of_unused_bottles;
    }

    public void setNo_of_unused_bottles(String no_of_unused_bottles) {
        this.no_of_unused_bottles = no_of_unused_bottles;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getCoupon_name() {
        return coupon_name;
    }

    public void setCoupon_name(String coupon_name) {
        this.coupon_name = coupon_name;
    }

    public String getCoupon_name_arabic() {
        return coupon_name_arabic;
    }

    public void setCoupon_name_arabic(String coupon_name_arabic) {
        this.coupon_name_arabic = coupon_name_arabic;
    }

    public String getPack_price() {
        return pack_price;
    }

    public void setPack_price(String pack_price) {
        this.pack_price = pack_price;
    }

    public String getPack_validity() {
        return pack_validity;
    }

    public void setPack_validity(String pack_validity) {
        this.pack_validity = pack_validity;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_name_arabic() {
        return product_name_arabic;
    }

    public void setProduct_name_arabic(String product_name_arabic) {
        this.product_name_arabic = product_name_arabic;
    }


    public CouponsModel(String total_coupons, String no_of_used_bottles, String no_of_unused_bottles, String expiry, String coupon_name, String coupon_name_arabic, String pack_price, String pack_validity, String product_name, String product_name_arabic) {
        this.total_coupons = total_coupons;
        this.no_of_used_bottles = no_of_used_bottles;
        this.no_of_unused_bottles = no_of_unused_bottles;
        this.expiry = expiry;
        this.coupon_name = coupon_name;
        this.coupon_name_arabic = coupon_name_arabic;
        this.pack_price = pack_price;
        this.pack_validity =pack_validity;
        this.product_name = product_name;
        this.product_name_arabic = product_name_arabic;
    }
}
