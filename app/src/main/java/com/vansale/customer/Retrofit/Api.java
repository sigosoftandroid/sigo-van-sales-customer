package com.vansale.customer.Retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Api {


    @FormUrlEncoded
    @POST("android/customer/CheckMobile")
    Call<ResponseBody> checkMobile(@Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("android/customer/login/send_otp")
    Call<ResponseBody> sendOtp(@Field("mobile") String phone,
                               @Field("otp") String otp);

    @FormUrlEncoded
    @POST("android/customer/register")
    Call<ResponseBody> register(@Field("name") String name, @Field("mobile") String mobile, @Field("email") String email, @Field("password") String password, @Field("image") String image, @Field("address") String address, @Field("trn") String trn);

    @FormUrlEncoded
    @POST("android/customer/login")
    Call<ResponseBody> login(@Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("android/customer/Profile")
    Call<ResponseBody> getUserDetails(@Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("android/customer/profile/getAddress")
    Call<ResponseBody> getAddress(@Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("android/customer/update")
    Call<ResponseBody> editCustomer(@Field("customer_id") String user_id, @Field("name") String name_latin,@Field("email")  String email,@Field("mobile") String mobile, @Field("profile") String image, @Field("address") String address, @Field("trn") String trn);

    @FormUrlEncoded
    @POST("android/customer/products")
    Call<ResponseBody> getProducts(@Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("android/customer/cart")
    Call<ResponseBody> updateQty(@Field("customer_id") String customer_id, @Field("product_id") String product_id, @Field("quantity") String quantity);

    @FormUrlEncoded
    @POST("android/customer/cart/getCartData")
    Call<ResponseBody> getCartData(@Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("android/customer/cart/deleteCartData")
    Call<ResponseBody> deleteCartData(@Field("cart_id") String cart_id);

    @FormUrlEncoded
    @POST("android/customer/cart/updateCart")
    Call<ResponseBody> updateCartData(@Field("customer_id") String customer_id, @Field("cart_id") String cart_id, @Field("quantity") String quantity);

    @FormUrlEncoded
    @POST("android/customer/orders/placeOrder")
    Call<ResponseBody> placeOrder(@Field("customer_id") String customer_id, @Field("delivery_date") String delivery_date, @Field("delivery_time") String delivery_time, @Field("building") String building, @Field("housenumber") String housenumber,@Field("latitude") String latitude,@Field("longitude") String longitude,@Field("location") String location,@Field("payment_method") String payment_method);

    @FormUrlEncoded
    @POST("android/customer/orders/getOrders")
    Call<ResponseBody> getOrders(@Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("android/customer/orders/getOrderDetails")
    Call<ResponseBody> getOrderDetails(@Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("android/customer/coupons")
    Call<ResponseBody> getActiveCoupons(@Field("customer_id") String customer_id);

}
