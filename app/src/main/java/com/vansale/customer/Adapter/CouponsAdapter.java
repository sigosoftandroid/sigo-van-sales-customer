package com.vansale.customer.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vansale.customer.BaseClass;
import com.vansale.customer.Model.CouponsModel;
import com.vansale.customer.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CouponsAdapter extends RecyclerView.Adapter<CouponsAdapter.MyViewHolder> {

    private List<CouponsModel> couponsList;
    Context con;

    BaseClass baseClass = new BaseClass();
    Typeface typeface,typeface1;
    String app_language;

    public CouponsAdapter(Context context, List<CouponsModel> couponslist) {

        this.con = context;
        this.couponsList = couponslist;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_couponname,txt_producttype,txt_noofbottles,txt_usedbottles,txt_unusedbottles,txt_expiry;

        public MyViewHolder(View view) {
            super(view);

            txt_couponname = view.findViewById(R.id.txt_couponname);
            txt_producttype = view.findViewById(R.id.txt_producttype);
            txt_noofbottles = view.findViewById(R.id.txt_noofbottles);
            txt_unusedbottles = view.findViewById(R.id.txt_unusedbottles);
            txt_usedbottles = view.findViewById(R.id.txt_usedbottles);
            txt_expiry = view.findViewById(R.id.txt_expiry);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_coupon, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final CouponsModel model = couponsList.get(position);


        holder.txt_usedbottles.setText(model.getNo_of_used_bottles());
        holder.txt_unusedbottles.setText(model.getNo_of_unused_bottles());
        holder.txt_noofbottles.setText(model.getTotal_coupons());
        holder.txt_expiry.setText(parseDateToddMMyyyy(model.getExpiry()));




        app_language = baseClass.getSharedPreferance(con, "selectedLanguage", "English");

        if(app_language.equals("Arabic")){
            holder.txt_producttype.setText(model.getProduct_name_arabic());
            holder.txt_couponname.setText(model.getCoupon_name_arabic());
        }
        else {
            holder.txt_producttype.setText(model.getProduct_name());
            holder.txt_couponname.setText(model.getCoupon_name());
        }

    }

    @Override
    public int getItemCount() {
        return couponsList.size();
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


}
