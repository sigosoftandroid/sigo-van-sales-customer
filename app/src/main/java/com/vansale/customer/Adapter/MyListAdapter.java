package com.vansale.customer.Adapter;

/**
 * Created by HP Laptops on 16-08-2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vansale.customer.Activity.OrderDetailActivity;
import com.vansale.customer.Model.DetailInfo;
import com.vansale.customer.Model.HeaderModel;
import com.vansale.customer.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MyListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<HeaderModel> deptList;

    public MyListAdapter(Context context, ArrayList<HeaderModel> deptList) {
        this.context = context;
        this.deptList = deptList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<DetailInfo> productList =
                deptList.get(groupPosition).getProductList();
        return productList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View view, ViewGroup parent) {

        final DetailInfo detailInfo = (DetailInfo) getChild(groupPosition, childPosition);
        if (view == null) {
            LayoutInflater infalInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.child_row, null);
        }
        LinearLayout rel = (LinearLayout) view.findViewById(R.id.lin);
        TextView sequence = (TextView) view.findViewById(R.id.txt_slno);
        TextView date = (TextView) view.findViewById(R.id.txt_date);
        TextView invoice = view.findViewById(R.id.txt_invoice);
        sequence.setText(detailInfo.getSequence().trim() + ") ");
        TextView  txt_status = view.findViewById(R.id.txt_status);
        invoice.setText("INV"+detailInfo.getName());
        txt_status.setText(detailInfo.getStatus());

        String input = detailInfo.getDate().trim();
        Log.e("date",input);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputformat = new SimpleDateFormat("dd MMMM, yyyy");
        Date date2 = null;
        String output = null;
        try{
            //Conversion of input String to date
            date2= df.parse(input);
            //old date format to new date format
            output = outputformat.format(date2);
            System.out.println(output);
        }catch(ParseException pe){
            pe.printStackTrace();
        }

       rel.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Log.e("orderid",detailInfo.getName());
               context.startActivity(new Intent(context,OrderDetailActivity.class).putExtra("order_id",detailInfo.getName()));
               ((Activity)context).finish();
           }
       });



        date.setText(output);
        //childItem.setText(detailInfo.getName().trim());
        return view;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        ArrayList<DetailInfo> productList =
                deptList.get(groupPosition).getProductList();
        return productList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return deptList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return deptList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {

        HeaderModel headerInfo = (HeaderModel) getGroup(groupPosition);
        if (view == null) {
            LayoutInflater inf = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inf.inflate(R.layout.group_heading, null);
        }

        TextView heading = (TextView) view.findViewById(R.id.heading);
        heading.setText(headerInfo.getName().trim());

        return view;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}