package com.vansale.customer.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

public class DashboardAdapter extends BaseAdapter {

    private ArrayList<String> mOptions = new ArrayList<>();
    private ArrayList<com.vansale.customer.views.DuoOptionView> mOptionViews = new ArrayList<>();
    private ArrayList<Integer> mTitlesIcon = new ArrayList();

    Context context;


    public DashboardAdapter(Context context,ArrayList<String> menuoptions, ArrayList<Integer> menuicons) {

        this.mOptions = menuoptions;

        this.mTitlesIcon = menuicons;

        this.context = context;

    }

    @Override
    public int getCount() {
        return  mOptions.size();
    }

    @Override
    public Object getItem(int position) {
        return mOptions.get(position);
    }

    public void setViewSelected(int position, boolean selected) {

        // Looping through the options in the menu
        // Selecting the chosen option
        for (int i = 0; i < mOptionViews.size(); i++) {
            if (i == position) {
                mOptionViews.get(i).setSelected(selected);
            } else {
                mOptionViews.get(i).setSelected(!selected);
            }
        }
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final String option = mOptions.get(position);
        final  Integer mTitleIcons = mTitlesIcon.get(position);
        final com.vansale.customer.views.DuoOptionView optionView;;

        // Using the DuoOptionView to easily recreate the demo
       // final DuoOptionView optionView;
        if (convertView == null) {
           // optionView = new DuoOptionView(parent.getContext());
            optionView = new com.vansale.customer.views.DuoOptionView(parent.getContext());
        } else {
            optionView = (com.vansale.customer.views.DuoOptionView) convertView;
        }


        // Using the DuoOptionView's default selectors
        optionView.bind(option, mTitleIcons,position);

        // Adding the views to an array list to handle view selection
        mOptionViews.add(optionView);

        return optionView;
    }
}
