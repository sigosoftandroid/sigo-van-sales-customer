package com.vansale.customer.Adapter;

import android.content.Context;
import android.graphics.Typeface;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.vansale.customer.Retrofit.Api;
import com.vansale.customer.BaseClass;
import com.vansale.customer.Model.ProductModel;
import com.vansale.customer.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {

    private List<ProductModel> productList;
    Context con;

    BaseClass baseClass = new BaseClass();
    Typeface typeface,typeface1;

    String categories, customer_id;
    String app_language;

    public ProductAdapter(Context context, List<ProductModel> productsviewlist) {

        this.con = context;
        this.productList = productsviewlist;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView product_image;
        ImageView plus, minus;
        TextView product_name;
        TextView weight;
        TextView price,txt_couponcount;
        TextView qty;
        ProgressBar progressBar;
        LinearLayout linearLayout;


        public MyViewHolder(View view) {
            super(view);

            product_name = view.findViewById(R.id.productname_txt);
            price = view.findViewById(R.id.price_txt);
            qty = view.findViewById(R.id.qty_txt);
            plus = view.findViewById(R.id.img_plus);
            minus = view.findViewById(R.id.img_minus);
            product_image = view.findViewById(R.id.product_img);
            progressBar = view.findViewById(R.id.progressbar);
            txt_couponcount = view.findViewById(R.id.txt_couponcount);
            linearLayout = view.findViewById(R.id.lin_product);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_product, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProductModel model = productList.get(position);
        holder.price.setText("AED " + model.getPrice());
        holder.qty.setText(model.getQuantity());

        Picasso.with(con).load(model.getImage()).placeholder(R.drawable.noimage3).into(holder.product_image, new Callback() {
            @Override
            public void onSuccess() {
                holder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });


        //setting typeface
        typeface = Typeface.createFromAsset(con.getAssets(), "fonts/AvenirLTStd_Roman.ttf");
        holder.product_name.setTypeface(typeface);
        typeface1 = Typeface.createFromAsset(con.getAssets(), "fonts/Avenir_Heavy.ttf");
        holder.price.setTypeface(typeface1);


        categories = baseClass.getSharedPreferance(con, "category", "");
        app_language = baseClass.getSharedPreferance(con, "selectedLanguage", "English");

        if(app_language.equals("Arabic")){
            holder.product_name.setText(model.getProduct_name_arabic());
        }
        else {
            holder.product_name.setText(model.getProduct_name());
        }

        if(model.getCoupon().equals("null")){

            holder.txt_couponcount.setText("0");
            model.setCoupon("0");
        }else {
            holder.txt_couponcount.setText(model.getCoupon());
        }

        // plus click
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int quantity = Integer.parseInt(holder.qty.getText().toString());
                if(Integer.parseInt(model.getCoupon())>Integer.parseInt(model.getStock())){
                    if(quantity>=Integer.parseInt(model.getStock())) {
                        Toast.makeText(con, "Stock limit reached", Toast.LENGTH_SHORT).show();
                    }else {
                        int q = quantity + 1;
                        String qtys = String.valueOf(q);
                        String id = model.getProduct_id();
                        holder.qty.setText(qtys);
                        customer_id = baseClass.getSharedPreferance(con, "UserId", "0");
                        UpdateQty(customer_id, id, qtys);
                    }
                }else if(!model.getCoupon().equals("0")){
                    if(Integer.parseInt(model.getStock())>Integer.parseInt(model.getCoupon())){
                        if(quantity>=Integer.parseInt(model.getCoupon())) {
                            Toast.makeText(con, "Coupon limit reached", Toast.LENGTH_SHORT).show();
                        }else {
                            int q = quantity + 1;
                            String qtys = String.valueOf(q);
                            String id = model.getProduct_id();
                            holder.qty.setText(qtys);
                            customer_id = baseClass.getSharedPreferance(con, "UserId", "0");
                            UpdateQty(customer_id, id, qtys);
                        }
                }

                }else  if(model.getCoupon().equals("0")){
                    if(quantity>=Integer.parseInt(model.getStock())) {
                        Toast.makeText(con, "Stock limit reached", Toast.LENGTH_SHORT).show();
                    }else {
                        int q = quantity + 1;
                        String qtys = String.valueOf(q);
                        String id = model.getProduct_id();
                        holder.qty.setText(qtys);
                        customer_id = baseClass.getSharedPreferance(con, "UserId", "0");
                        UpdateQty(customer_id, id, qtys);
                    }
                }
                else {
                    int q = quantity + 1;
                    String qtys = String.valueOf(q);
                    String id = model.getProduct_id();
                    holder.qty.setText(qtys);
                    customer_id = baseClass.getSharedPreferance(con, "UserId", "0");
                    UpdateQty(customer_id, id, qtys);
                }
            }
        });
        // minus click
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = Integer.parseInt(holder.qty.getText().toString());
                if (quantity == 0) {
                    int q = quantity - 0;
                } else {
                    int q = quantity - 1;
                    String qtys = String.valueOf(q);
                    String id = model.getProduct_id();
                    holder.qty.setText(qtys);
                    customer_id = baseClass.getSharedPreferance(con,"UserId","0");
                    UpdateQty(customer_id, id, qtys);
                }
            }
        });

        }

    @Override
    public int getItemCount() {
        return productList.size();
    }



    //add products
    public void UpdateQty(final String customer_id, final String id, final String qtys) {

        Api api = BaseClass.getApi();
        Call<ResponseBody> call = api.updateQty(customer_id,id,qtys);
        call.enqueue(new retrofit2.Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    String s = null;
                    try {
                        s = response.body().string();
                        System.out.println("Response.........." + s);
                        JSONObject jsonObject = new JSONObject(s);
                        String message = jsonObject.getString("message");
                        if (message.equals("success")) {

                        }

                    } catch (IOException e) {
                        e.printStackTrace();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{

                    Toast.makeText(con, "Something went wrong", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(con, "Connection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


    }



}

