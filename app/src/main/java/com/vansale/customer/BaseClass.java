package com.vansale.customer;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.vansale.customer.Retrofit.Api;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseClass {

    public static String mainURL = "https://moongardens.in/vansale/";

    public void setSharedPreferance(Context con, String key, String value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(con);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.commit();


    }

    public String getSharedPreferance(Context con, String key, String value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(con);
        String result = sp.getString(key, value);
        return result;
    }


    private static Retrofit getRetrofitInstance() {

        return new Retrofit.Builder()
                .baseUrl(mainURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }


    public static Api getApi() {
        return getRetrofitInstance().create(Api.class);
    }
}
