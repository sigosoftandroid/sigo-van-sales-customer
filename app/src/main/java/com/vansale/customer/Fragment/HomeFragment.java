package com.vansale.customer.Fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vansale.customer.Activity.ProductActivity;
import com.vansale.customer.BaseClass;
import com.vansale.customer.R;


public class HomeFragment extends Fragment   {

    LinearLayout lin_order;
    TextView tv_order;
    Typeface typeface;
    BaseClass baseClass = new BaseClass();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);

        lin_order = (LinearLayout) view.findViewById(R.id.ll_order);
        tv_order = (TextView)view.findViewById(R.id.txt_order);

        //setting typeface
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/AvenirLTStd_Roman.ttf");
        tv_order.setTypeface(typeface);

        lin_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                baseClass.setSharedPreferance(getActivity(),"category","Domestic");
                startActivity(new Intent(getActivity(),ProductActivity.class));
            }
        });


        return view;
    }


}
