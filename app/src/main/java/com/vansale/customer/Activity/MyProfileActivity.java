package com.vansale.customer.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.vansale.customer.Retrofit.Api;
import com.vansale.customer.BaseClass;
import com.vansale.customer.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class MyProfileActivity extends AppCompatActivity {

    TextView txt_name_latin,txt_phone, txt_email,tv_trn;
    TextView lbl_name_latin,lbl_phone, lbl_email;
    Button btn_edit;
    CircleImageView profile_image;
    String user_id;
    String name_latin,email, mobile,image;
    BaseClass baseClass = new BaseClass();
    Typeface typeface;
    TextView tv_address,lbl_address,lbl_trn;
    String address,trn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(MyProfileActivity.this);
        setContentView(R.layout.activity_my_profile);
        getSupportActionBar().hide();

        txt_name_latin = findViewById(R.id.tv_name_latin);
        txt_phone = findViewById(R.id.tv_phone);
        txt_email = findViewById(R.id.tv_email);
        tv_address = findViewById(R.id.tv_address);
        tv_trn = findViewById(R.id.tv_trn);
        lbl_name_latin = findViewById(R.id.lbl_name_latin);
        lbl_phone = findViewById(R.id.lbl_phone);
        lbl_email = findViewById(R.id.lbl_email);
        lbl_address = findViewById(R.id.lbl_address);
        lbl_trn = findViewById(R.id.lbl_trn);

        btn_edit = findViewById(R.id.btn_edit);

        profile_image = findViewById(R.id.iv_profile);

        //setting typeface
        typeface = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");


        txt_name_latin.setTypeface(typeface);
        txt_phone.setTypeface(typeface);
        txt_email.setTypeface(typeface);
        tv_address.setTypeface(typeface);
        tv_trn.setTypeface(typeface);

        lbl_name_latin.setTypeface(typeface);
        lbl_phone.setTypeface(typeface);
        lbl_email.setTypeface(typeface);
        lbl_address.setTypeface(typeface);
        lbl_trn.setTypeface(typeface);

        user_id = baseClass.getSharedPreferance(getBaseContext(), "UserId", "0");

        GetUserDetails();

    }

    //color for status bar
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient_toolbar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    //service for checking mobile number exist
    public void GetUserDetails() {
        if(NetworkConnection.isNetworkStatusAvialable(MyProfileActivity.this)) {
            final ProgressDialog dialog = new ProgressDialog(MyProfileActivity.this);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
            Api api = BaseClass.getApi();
            Call<ResponseBody> call = api.getUserDetails(user_id);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                    if (response.isSuccessful()) {

                        dialog.dismiss();
                        String s = null;
                        try {
                            s = response.body().string();
                            System.out.println("Response.........." + s);
                            JSONObject jo = new JSONObject(s);

                            String status = jo.getString("message");

                            if (status.equals("success")) {

                                JSONObject jsonObject = jo.getJSONObject("customer");
                                name_latin = jsonObject.getString("name");
                                email = jsonObject.getString("email");
                                mobile = jsonObject.getString("phone");
                                image = BaseClass.mainURL + jsonObject.getString("image");
                                address = jsonObject.getString("address");
                                trn = jsonObject.getString("trn");

                                Picasso.with(MyProfileActivity.this).load(image).error(R.drawable.user_default).into(profile_image);
                                txt_name_latin.setText(name_latin);
                                txt_phone.setText(mobile);
                                txt_email.setText(email);
                                tv_address.setText(address);
                                tv_trn.setText(trn);


                                baseClass.setSharedPreferance(getApplicationContext(), "Email", email);
                                baseClass.setSharedPreferance(getApplicationContext(), "Mobile", mobile);
                                baseClass.setSharedPreferance(getApplicationContext(), "Name", name_latin);
                                baseClass.setSharedPreferance(getApplicationContext(), "Image", image);
                                baseClass.setSharedPreferance(getApplicationContext(), "Address", address);
                                baseClass.setSharedPreferance(getApplicationContext(), "trn", trn);


                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {

                        dialog.dismiss();
                        Toast.makeText(MyProfileActivity.this, "Response error", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    dialog.dismiss();
                    Toast.makeText(MyProfileActivity.this, "Connection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }else {
            Toast.makeText(this, "Check your connection", Toast.LENGTH_SHORT).show();
        }

    }

    public void EditClick(View view) {
        if(mobile!=null) {
            Intent intent = new Intent(MyProfileActivity.this, EditProfileActivity.class);
            intent.putExtra("name_latin", name_latin);
//            intent.putExtra("name_arabic", name_arabic);
            intent.putExtra("mobile", mobile);
            intent.putExtra("email", email);
            intent.putExtra("image", image);
            intent.putExtra("address", address);
            intent.putExtra("trn",trn );

            startActivity(intent);
            finish();
        }

    }

    public void backClick(View view) {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
