package com.vansale.customer.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vansale.customer.Retrofit.Api;
import com.vansale.customer.Adapter.MyListAdapter;
import com.vansale.customer.BaseClass;
import com.vansale.customer.Model.DetailInfo;
import com.vansale.customer.Model.HeaderModel;
import com.vansale.customer.Model.MyOrdersModel;
import com.vansale.customer.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.StringTokenizer;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class MyOrdersActivity extends AppCompatActivity {

    ExpandableListView expandableListView;
    TextView txt_orders;

    BaseClass baseClass = new BaseClass();
    MyOrdersModel myOrdersModel;
    MyListAdapter listAdapter;

    String customer_id;
    ArrayList<MyOrdersModel> myorderList = new ArrayList<>();
    private ArrayList<HeaderModel> SectionList = new ArrayList<>();
    private LinkedHashMap<String, HeaderModel> mySection = new LinkedHashMap<>();
    ImageView iv_noData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(MyOrdersActivity.this);
        setContentView(R.layout.activity_my_orders);
        expandableListView = findViewById(R.id.expandablelistview);
        txt_orders = findViewById(R.id.txt_orders);
        iv_noData = findViewById(R.id.iv_noData);

        getSupportActionBar().hide();

        customer_id = baseClass.getSharedPreferance(getApplicationContext(),"UserId","0");
        Log.e("customer_id",customer_id);
        MyOrders(customer_id);


        listAdapter = new MyListAdapter(this, SectionList);
        expandableListView.setAdapter(listAdapter);


    }

    public void backClick(View view) {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    // set status bar
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient_toolbar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    public void MyOrders(final String customer_id) {
        if(NetworkConnection.isNetworkStatusAvialable(MyOrdersActivity.this)) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage(getString(R.string.loading_dialog));
            dialog.setCancelable(false);
            dialog.show();
            Api api = BaseClass.getApi();
            Call<ResponseBody> call = api.getOrders(customer_id);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                    if (response.isSuccessful()) {
                        dialog.dismiss();
                        String s = null;
                        try {
                            s = response.body().string();
                            System.out.println("Response.........." + s);
                            JSONObject json = new JSONObject(s);

                            String status = json.getString("message");
                            if (status.equals("success")) {
                                JSONArray jsonArray = json.getJSONArray("orders");
                                if (jsonArray.length() == 0) {
                                    Toast.makeText(MyOrdersActivity.this, "No orders found", Toast.LENGTH_SHORT).show();
                                    iv_noData.setVisibility(View.VISIBLE);
                                } else {
                                    iv_noData.setVisibility(View.GONE);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        String order_id = jsonObject.getString("order_id");
                                        String ordered_date = jsonObject.getString("ordered_date");
                                        String delivery_status = jsonObject.getString("status");
                                        String timestamp = jsonObject.getString("timestamp");

                                        myOrdersModel = new MyOrdersModel(order_id, ordered_date, delivery_status, timestamp);
                                        myorderList.add(myOrdersModel);
                                    }
                                    String yesterdayAsString = null;
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                    String currentDateandTime = sdf.format(new Date());
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                                    Date date = null;
                                    try {
                                        date = dateFormat.parse(currentDateandTime);
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.setTime(date);
                                        calendar.add(Calendar.DATE, -1);
                                        yesterdayAsString = dateFormat.format(calendar.getTime());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    if (myorderList.size() > 0) {

                                        txt_orders.setVisibility(View.GONE);
                                        expandableListView.setVisibility(View.VISIBLE);

                                        for (int i = 0; i < myorderList.size(); i++) {
//                                    StringTokenizer tk = new StringTokenizer(myorderList.get(i).getDate());
                                            String dateString1 = myorderList.get(i).getTimestamp();
                                            StringTokenizer tk = new StringTokenizer(dateString1);

                                            String dateString = tk.nextToken();  // <---  yyyy-mm-dd
                                            String time = tk.nextToken();  // <---  hh:mm:ss
                                            Log.e("dataraay", dateString);

                                            String[] arrayString = dateString.split("-");
                                            int yearCurrent = Integer.parseInt(arrayString[0]);
                                            int monthYear = Integer.parseInt(arrayString[1]);


                                            Calendar c = Calendar.getInstance();
                                            int year = c.get(Calendar.YEAR);
                                            int month = c.get(Calendar.MONTH) + 1;


                                            if (dateString.equals(currentDateandTime)) {
                                                addProduct(getString(R.string.today), myorderList.get(i).getRequest_id(), myorderList.get(i).getDate(), myorderList.get(i).getDelivery_status());
                                            } else if (dateString.equals(yesterdayAsString)) {
                                                addProduct(getString(R.string.yesterday), myorderList.get(i).getRequest_id(), myorderList.get(i).getDate(), myorderList.get(i).getDelivery_status());
                                            } else if (yearCurrent == year && monthYear == month) {
                                                addProduct(getString(R.string.this_month), myorderList.get(i).getRequest_id(), myorderList.get(i).getDate(), myorderList.get(i).getDelivery_status());
                                            } else {
                                                addProduct(getString(R.string.last_months), myorderList.get(i).getRequest_id(), myorderList.get(i).getDate(), myorderList.get(i).getDelivery_status());
                                            }
                                        }
                                    } else {
                                        txt_orders.setVisibility(View.VISIBLE);
                                        expandableListView.setVisibility(View.GONE);
                                    }

                                    listAdapter.notifyDataSetChanged();
                                }
                            } else if (status.equals("failed")) {
                                Toast.makeText(MyOrdersActivity.this, R.string.api_error, Toast.LENGTH_SHORT).show();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {

                        Toast.makeText(MyOrdersActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Toast.makeText(MyOrdersActivity.this, "Connection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            });

        }else {
            Toast.makeText(this, "Check your connection", Toast.LENGTH_SHORT).show();
        }


    }

    private int addProduct(String department, String product, String date,String status) {
        int groupPosition = 0;
        //check the hash map if the group already exists
        HeaderModel headerInfo = mySection.get(department);
        //add the group if doesn't exists
        if (headerInfo == null) {
            headerInfo = new HeaderModel();
            headerInfo.setName(department);
            mySection.put(department, headerInfo);
            SectionList.add(headerInfo);
        }
        //get the children for the group
        ArrayList<DetailInfo> productList = headerInfo.getProductList();
        //size of the children list
        int listSize = productList.size();
        //add to the counter
        listSize++;
        //create a new child and add that to the group
        DetailInfo detailInfo = new DetailInfo();
        detailInfo.setSequence(String.valueOf(listSize));
        detailInfo.setName(product);
        detailInfo.setDate(date);
        detailInfo.setStatus(status);
        productList.add(detailInfo);
        headerInfo.setProductList(productList);
        //find the group position inside the list
        groupPosition = SectionList.indexOf(headerInfo);
        return groupPosition;
    }
}
