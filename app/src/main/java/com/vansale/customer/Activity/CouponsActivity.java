package com.vansale.customer.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.vansale.customer.Adapter.CouponsAdapter;
import com.vansale.customer.BaseClass;
import com.vansale.customer.Model.CouponsModel;
import com.vansale.customer.R;
import com.vansale.customer.Retrofit.Api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class CouponsActivity extends AppCompatActivity {
  RecyclerView recycler_view;
  BaseClass baseClass = new BaseClass();
  String customer_id;
  List<CouponsModel> couponsList = new ArrayList<>();
  CouponsModel couponsModel;
  CouponsAdapter couponsAdapter;
  LinearLayout lin_nodata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(CouponsActivity.this);
        setContentView(R.layout.activity_coupons);
        getSupportActionBar().hide();

        recycler_view = findViewById(R.id.recycler_view);
        lin_nodata = findViewById(R.id.lin_nodata);

        customer_id = baseClass.getSharedPreferance(CouponsActivity.this,"UserId","0");
        getActiveCoupons(customer_id);

    }

    public void getActiveCoupons(final String customer_id) {
        if(NetworkConnection.isNetworkStatusAvialable(CouponsActivity.this)) {
            final ProgressDialog dialog = new ProgressDialog(CouponsActivity.this);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
            Api api = BaseClass.getApi();
            Call<ResponseBody> call = api.getActiveCoupons(customer_id);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                    if (response.isSuccessful()) {
                        dialog.dismiss();
                        String s = null;
                        try {
                            s = response.body().string();
                            System.out.println("Response.........." + s);
                            JSONObject jo = new JSONObject(s);

                            String message = jo.getString("message");
                            if (message.equals("success")) {
                                couponsList.clear();

                                JSONArray jsonArray = jo.getJSONArray("data");
                                if (jsonArray.length() == 0) {
                                    lin_nodata.setVisibility(View.VISIBLE);
//                                    Toast.makeText(CouponsActivity.this, "No Active coupons", Toast.LENGTH_SHORT).show();
                                } else {
                                    lin_nodata.setVisibility(View.GONE);
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        String total_coupons = jsonObject.getString("totalCoupons");
                                        String no_of_used_bottles = jsonObject.getString("totalUsed");
                                        String no_of_unused_bottles = jsonObject.getString("totalUnUsed");
                                        String expiry = jsonObject.getString("expiry_date");
                                        String coupon_name = jsonObject.getString("pack_name");
                                        String coupon_name_arabic = jsonObject.getString("pack_name_arabic");
                                        String pack_price = jsonObject.getString("pack_price");
                                        String pack_validity = jsonObject.getString("pack_validity");
                                        String product_name = jsonObject.getString("product_name");
                                        String product_name_arabic = jsonObject.getString("product_name_arabic");

                                        couponsModel = new CouponsModel(total_coupons, no_of_used_bottles, no_of_unused_bottles, expiry, coupon_name, coupon_name_arabic, pack_price, pack_validity, product_name, product_name_arabic);
                                        couponsList.add(couponsModel);

                                    }
                                    couponsAdapter = new CouponsAdapter(CouponsActivity.this, couponsList);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                    recycler_view.setLayoutManager(mLayoutManager);
                                    recycler_view.setItemAnimator(new DefaultItemAnimator());
                                    recycler_view.setAdapter(couponsAdapter);
                                }
                            } else {

                                Toast.makeText(CouponsActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                            }


                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {

                        Toast.makeText(CouponsActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dialog.dismiss();
                    Toast.makeText(CouponsActivity.this, "Connection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }else {
            Toast.makeText(this, "Check your connection", Toast.LENGTH_SHORT).show();
        }


    }

    //color for status bar
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient_toolbar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void backClick(View view) {
        onBackPressed();
    }
}
