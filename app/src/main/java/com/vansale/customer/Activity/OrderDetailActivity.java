package com.vansale.customer.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.vansale.customer.Retrofit.Api;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.vansale.customer.BaseClass;
import com.vansale.customer.Model.OrderDetailPdtModel;
import com.vansale.customer.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class OrderDetailActivity extends AppCompatActivity {

    Intent intent;
    String order_id, delivery_status, ordered_date, grand_total;
    String buildings, houses, feedback,location,customer_id,credit_balance,agency_assigned,ordered_time,start_time;

    TextView order_date, billno, order_status, order_total,txt_couponstatus,txt_creditbalance;
    TextView txt_location, txt_housenumber, txt_building;
    TextView txt_ordertime,txt_drivername,txt_starttime;
    RecyclerView recyclerView;
    LinearLayout cancel_order,lin_drivername,lin_starttime;

    OrderDetailPdtModel orderDetailPdtModel;
    OrderDetailAdapter orderDetailAdapter;

    List<OrderDetailPdtModel> orderPdtList = new ArrayList<>();
    BaseClass baseClass = new BaseClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(OrderDetailActivity.this);
        setContentView(R.layout.activity_order_detail);

        getSupportActionBar().hide();

        order_date = findViewById(R.id.txt_orderdate);
        billno = findViewById(R.id.txt_billno);
        order_status = findViewById(R.id.txt_orderstatus);
        txt_couponstatus = findViewById(R.id.txt_couponstatus);
        txt_ordertime = findViewById(R.id.txt_ordertime);
        txt_drivername = findViewById(R.id.txt_drivername);
        txt_starttime = findViewById(R.id.txt_starttime);
        txt_housenumber = findViewById(R.id.txt_housenumber);
        txt_building = findViewById(R.id.txt_building);
        order_total = findViewById(R.id.txt_ordertotal);
        txt_creditbalance = findViewById(R.id.txt_creditbalance);
        txt_location = findViewById(R.id.txt_location);
        recyclerView = findViewById(R.id.recycler_view);
        cancel_order = findViewById(R.id.ll_cancel_order);
        lin_drivername = findViewById(R.id.lin_drivername);
        lin_starttime = findViewById(R.id.lin_starttime);

        customer_id = baseClass.getSharedPreferance(OrderDetailActivity.this, "UserId", "0");

        intent = getIntent();
        order_id = intent.getStringExtra("order_id");
        billno.setText("INV" + order_id);
        OrderDetails(order_id);
    }

    public void backClick(View view) {
        Intent intent = new Intent(OrderDetailActivity.this, MyOrdersActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(OrderDetailActivity.this, MyOrdersActivity.class);
        startActivity(intent);
        finish();
    }

    // set status bar
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient_toolbar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    public void OrderDetails(final String order_id) {
        if(NetworkConnection.isNetworkStatusAvialable(OrderDetailActivity.this)) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage(getString(R.string.loading_dialog));
            dialog.setCancelable(false);
            dialog.show();
            Api api = BaseClass.getApi();
            Call<ResponseBody> call = api.getOrderDetails(order_id);
            call.enqueue(new retrofit2.Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                    if (response.isSuccessful()) {

                        dialog.dismiss();
                        String s = null;
                        try {
                            s = response.body().string();
                            System.out.println("Response.........." + s);
                            JSONObject jo = new JSONObject(s);

                            String status = jo.getString("message");
                            if (status.equals("success")) {

                                JSONObject jsonObject = jo.getJSONObject("order");
                                ordered_date = jsonObject.getString("ordered_date");
                                ordered_time = jsonObject.getString("ordered_time");
                                delivery_status = jsonObject.getString("status");
                                agency_assigned = jsonObject.getString("agency");
                                start_time =  jsonObject.getString("start_time");
                                location = jsonObject.getString("location");
                                grand_total = jsonObject.getString("total");
                                credit_balance = jsonObject.getString("credit_balance");

                                txt_ordertime.setText(ordered_time);
                                if(delivery_status.equals("pending")){
                                   txt_drivername.setText("Not assigned");
                                    txt_starttime.setText("Not started");
                                }else {
                                    lin_drivername.setVisibility(View.VISIBLE);
                                    lin_starttime.setVisibility(View.VISIBLE);

                                    try {
                                        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                        final Date dateObj = sdf.parse(start_time);
                                        System.out.println(dateObj);
                                        String time = new SimpleDateFormat("K:mm a").format(dateObj);
                                        txt_starttime.setText(time);
                                    } catch (final ParseException e) {
                                        e.printStackTrace();
                                    }



                                    txt_drivername.setText(agency_assigned);
                                }

                                JSONObject jsonObject1 = jo.getJSONObject("address");
                                String address_id = jsonObject1.getString("address_id");
                                buildings = jsonObject1.getString("building");
                                houses = jsonObject1.getString("house");

                                JSONArray jsonArray = jo.getJSONArray("products");
                                for (int j = 0; j < jsonArray.length(); j++) {

                                    JSONObject jsonObject2 = jsonArray.getJSONObject(j);
                                    String product_id = jsonObject2.getString("product_id");
                                    String name_latin = jsonObject2.getString("product_name");
                                    String name_arabic = jsonObject2.getString("product_name_arabic");
                                    String quantity = jsonObject2.getString("quantity");
                                    String total = jsonObject2.getString("total");
                                    String coupon_applied = jsonObject2.getString("coupon_applied");
                                    String image = BaseClass.mainURL + jsonObject2.getString("product_image");

                                    orderDetailPdtModel = new OrderDetailPdtModel(product_id, name_latin, name_arabic, quantity, total, image);
                                    orderPdtList.add(orderDetailPdtModel);

                                    if(coupon_applied.equals("0")){
                                        txt_couponstatus.setText("Not Used");
                                    }else {
                                        txt_couponstatus.setText("Used");
                                    }

                                }

                                orderDetailAdapter = new OrderDetailAdapter(OrderDetailActivity.this, orderPdtList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                recyclerView.setLayoutManager(mLayoutManager);
                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                recyclerView.setAdapter(orderDetailAdapter);
                            }

                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                            DateFormat outputformat = new SimpleDateFormat("dd MMMM, yyyy");
                            Date date2 = null;
                            String output = null;
                            try {
                                //Conversion of input String to date
                                date2 = df.parse(ordered_date);
                                //old date format to new date format
                                output = outputformat.format(date2);
                                System.out.println(output);
                            } catch (ParseException pe) {
                                pe.printStackTrace();
                            }
                            order_date.setText(output);
                            order_status.setText(delivery_status);
                            if (delivery_status.equalsIgnoreCase("Pending")) {
                                cancel_order.setVisibility(View.VISIBLE);
                            }
                            order_total.setText("AED " + grand_total);
                            txt_creditbalance.setText("AED " +credit_balance);
                            houses = houses.replaceFirst("^\\s*", "");
                            buildings = buildings.replaceFirst("^\\s*", "");
                            location = location.replaceFirst("^\\s*", "");

                            txt_housenumber.setText(houses);
                            txt_building.setText(buildings);
                            txt_location.setText(location);

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {

                        Toast.makeText(OrderDetailActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Toast.makeText(OrderDetailActivity.this, "Connection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
                    dialog.dismiss();

                }
            });
        }else {
            Toast.makeText(this, "Check your connection", Toast.LENGTH_SHORT).show();
        }


    }

    public void cancelOrderClick(View view) {

        ShowCancelOrderDialog();

    }

    public void ShowCancelOrderDialog() {
        final MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .customView(R.layout.dialog_cancel_order, true)
                .cancelable(true)
                .autoDismiss(false);
        dialog = builder.build();
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final EditText et_reason = (EditText) dialog.findViewById(R.id.ed_reason);
        final Button btn_confirm = (Button) dialog.findViewById(R.id.btn_confirm);
        final Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String reason = et_reason.getText().toString();
                if (reason.length() == 0) {
                    Toast.makeText(OrderDetailActivity.this, R.string.specify_reason, Toast.LENGTH_SHORT).show();
                } else {

                    CancelOrder(reason, order_id);
                    dialog.dismiss();
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
    }


    //service for cancel order
    private void CancelOrder(final String reason, final String request_id) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.setCancelable(false);
        dialog.show();
        String url = BaseClass.mainURL + "android/customer/orders/cancelOrder";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String result0 = response.replaceAll("\\n", "");
                        String result = result0.replaceAll("\\n", "");
                        JSONObject jo = null;
                        try {
                            jo = new JSONObject(result);
                            String status = jo.getString("message");
                            if (status.equals("success")) {

                                Toast.makeText(OrderDetailActivity.this, R.string.order_cancelled, Toast.LENGTH_SHORT).show();
                                cancel_order.setVisibility(View.GONE);
                                order_status.setText("Cancelled");
                            }


                        } catch (JSONException e) {
                            Toast.makeText(OrderDetailActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }


                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), R.string.server_error, Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("customer_id",customer_id);
                params.put("order_id", request_id);
                params.put("reason", reason);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.MyViewHolder> {

        private List<OrderDetailPdtModel> productList;
        Context con;
        BaseClass baseClass = new BaseClass();
        String app_language;

        public OrderDetailAdapter(Context context, List<OrderDetailPdtModel> productsviewlist) {

            this.con = context;
            this.productList = productsviewlist;
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView pdt_image;
            TextView txt_pdtname;
            TextView txt_qty;
            TextView txt_total;


            public MyViewHolder(View view) {
                super(view);

                txt_pdtname = view.findViewById(R.id.txt_pdtname);
                txt_qty = view.findViewById(R.id.txt_qty);
                txt_total = view.findViewById(R.id.txt_total);
                pdt_image = view.findViewById(R.id.pdt_img);

            }
        }

        @Override
        public OrderDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_orderdetail, parent, false);
            return new OrderDetailAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final OrderDetailAdapter.MyViewHolder holder, final int position) {
            final OrderDetailPdtModel model = productList.get(position);

            app_language = baseClass.getSharedPreferance(con, "selectedLanguage", "English");

            if(app_language.equals("Arabic")){
                holder.txt_pdtname.setText(model.getName_arabic());
            }
            else {
                holder.txt_pdtname.setText(model.getName_latin());
            }




            holder.txt_total.setText("AED " + model.getTotal());
            holder.txt_qty.setText(model.getQuantity());

            Picasso.with(con).load(model.getImage()).placeholder(R.drawable.noimage3).into(holder.pdt_image, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {

                }
            });


        }

        @Override
        public int getItemCount() {
            return productList.size();
        }

    }
}
