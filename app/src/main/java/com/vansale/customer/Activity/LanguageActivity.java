package com.vansale.customer.Activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.vansale.customer.BaseClass;
import com.vansale.customer.R;

public class LanguageActivity extends AppCompatActivity {

    boolean isSlectedLanguageRTL = true;
    BaseClass baseClass = new BaseClass();

    LinearLayout lin_english, lin_arabic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        lin_arabic = findViewById(R.id.lin_arabic);
        lin_english = findViewById(R.id.lin_english);
    }

    public void submitClick(View view) {
    }
}
