package com.vansale.customer.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.vansale.customer.Retrofit.Api;
import com.vansale.customer.BaseClass;
import com.vansale.customer.Model.ProductModel;
import com.vansale.customer.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class CartActivity extends AppCompatActivity {

    CartAdapter cartAdapter;
    ProductModel productModel;
    BaseClass baseClass = new BaseClass();

    ImageView img_back;
    RecyclerView recyclerView;
    TextView toolbarTitle;
    TextView txt_total, lbl_total;
    Button btn_checkout;

    Typeface typeface, typeface1, typeface2, typeface3;

    List<ProductModel> cartList = new ArrayList<>();

    String customer_id, total;

    String app_language;
    LinearLayout lin_nodata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(CartActivity.this);
        setContentView(R.layout.activity_cart);
        getSupportActionBar().hide();

        img_back = findViewById(R.id.img_back);
        recyclerView = findViewById(R.id.recycler_view);
        toolbarTitle = findViewById(R.id.toolbar_title);
        txt_total = findViewById(R.id.txt_total);
        lbl_total = findViewById(R.id.lbl_total);
        btn_checkout = findViewById(R.id.btn_checkout);
        lin_nodata = findViewById(R.id.lin_nodata);

        app_language = baseClass.getSharedPreferance(getApplicationContext(), "selectedLanguage", "English");

        customer_id = baseClass.getSharedPreferance(CartActivity.this, "UserId", "0");
        cartAdapter = new CartAdapter(CartActivity.this, cartList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(cartAdapter);
        cartAdapter.notifyDataSetChanged();

        ViewCartProducts(customer_id);

        //setting typeface
        typeface = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        toolbarTitle.setTypeface(typeface);
        typeface1 = Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd_Roman.ttf");
        lbl_total.setTypeface(typeface1);
        typeface2 = Typeface.createFromAsset(getAssets(), "fonts/Avneir_Medium.ttf");
        txt_total.setTypeface(typeface2);
        typeface3 = Typeface.createFromAsset(getAssets(), "fonts/Avenir_LTD_Light.ttf");
        btn_checkout.setTypeface(typeface3);

        btn_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cartList.size()>0) {

                    baseClass.setSharedPreferance(CartActivity.this,"total",total);
                    startActivity(new Intent(CartActivity.this, AddressActivity.class));
                }
                else {
                    Toast.makeText(CartActivity.this, "Cart is empty", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    // set status bar
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient_toolbar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    //view cart products
    public void ViewCartProducts(final String customer_id) {
        if(NetworkConnection.isNetworkStatusAvialable(CartActivity.this)) {
            final ProgressDialog dialog = new ProgressDialog(CartActivity.this);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
            Api api = BaseClass.getApi();
            Call<ResponseBody> call = api.getCartData(customer_id);
            call.enqueue(new retrofit2.Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                    if (response.isSuccessful()) {
                        dialog.dismiss();
                        String s = null;
                        try {
                            s = response.body().string();
                            System.out.println("Response.........." + s);
                            JSONObject jo = new JSONObject(s);

                            String message = jo.getString("message");
                            total = jo.getString("total");
                            if (message.equals("success")) {

                                JSONArray jsonArray = jo.getJSONArray("products");
                                if (jsonArray.length() == 0) {
                                    Toast.makeText(CartActivity.this, "Cart is empty", Toast.LENGTH_SHORT).show();
                                    lin_nodata.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.GONE);
                                } else {
                                    lin_nodata.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        String product_id = jsonObject.getString("product_id");
                                        String product_name_english = jsonObject.getString("product_name_english");
                                        String product_name_arabic = jsonObject.getString("product_name_arabic");
                                        String cat_id = jsonObject.getString("cat_id");
                                        String price = jsonObject.getString("price");
                                        String product_vat = jsonObject.getString("product_vat");
                                        String image = BaseClass.mainURL + jsonObject.getString("product_image");
                                        String quantity = jsonObject.getString("quantity");
                                        String cart_id = jsonObject.getString("cart_id");
                                        String stock = jsonObject.getString("stock");
                                        String subtotal = jsonObject.getString("subtotal");

                                        productModel = new ProductModel(product_id, product_name_english, product_name_arabic, cat_id, price, product_vat, image, quantity, cart_id, stock, subtotal);
                                        cartList.add(productModel);
                                    }
                                    cartAdapter = new CartAdapter(CartActivity.this, cartList);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                    recyclerView.setLayoutManager(mLayoutManager);
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                                    recyclerView.setAdapter(cartAdapter);
                                    cartAdapter.notifyDataSetChanged();
                                    txt_total.setText("AED" + " " + total);
                                }
                            } else {

                                Toast.makeText(CartActivity.this, "Ooops !..Something went wrong", Toast.LENGTH_SHORT).show();
                            }


                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {

                        Toast.makeText(CartActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dialog.dismiss();
                    Toast.makeText(CartActivity.this, "Connection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }else {
            Toast.makeText(this, "Check your connection", Toast.LENGTH_SHORT).show();
        }


    }


    public void backClick(View view) {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {

        private List<ProductModel> productList;
        Context con;
        BaseClass baseClass = new BaseClass();
        Typeface typeface, typeface1;

        public CartAdapter(Context context, List<ProductModel> productsviewlist) {

            this.con = context;
            this.productList = productsviewlist;
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView product_image;
            ImageView plus, minus;
            TextView product_name;
            TextView weight;
            TextView price;
            TextView subtotal;
            TextView qty;
            LinearLayout linearLayout, lin_delete;
            ProgressBar progressBar;


            public MyViewHolder(View view) {
                super(view);

                product_name = (TextView) view.findViewById(R.id.productname_txt);
                weight = (TextView) view.findViewById(R.id.weight_txt);
                price = (TextView) view.findViewById(R.id.price_txt);
                qty = (TextView) view.findViewById(R.id.qty_txt);
                product_image = view.findViewById(R.id.product_img);
                plus = view.findViewById(R.id.img_plus);
                minus = view.findViewById(R.id.img_minus);
                progressBar = view.findViewById(R.id.progressbar);
                linearLayout = view.findViewById(R.id.lin_product);
                lin_delete = view.findViewById(R.id.lin_delete);
                subtotal = view.findViewById(R.id.txt_subtotal);

            }
        }

        @Override
        public CartAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_cart, parent, false);
            return new CartAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final CartAdapter.MyViewHolder holder, final int position) {
            final ProductModel model = productList.get(position);
            holder.price.setText("AED " + model.getPrice());
            holder.subtotal.setText("AED " + model.getSubtotal());
            holder.qty.setText(model.getQuantity());

            Picasso.with(con).load(model.getImage()).placeholder(R.drawable.noimage3).into(holder.product_image, new Callback() {
                @Override
                public void onSuccess() {
                    holder.progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {

                }
            });


            //setting typeface
            typeface = Typeface.createFromAsset(con.getAssets(), "fonts/AvenirLTStd_Roman.ttf");
            holder.product_name.setTypeface(typeface);
            typeface1 = Typeface.createFromAsset(con.getAssets(), "fonts/Avenir_Heavy.ttf");
            holder.price.setTypeface(typeface1);
            holder.weight.setTypeface(typeface1);

            app_language = baseClass.getSharedPreferance(con, "selectedLanguage", "English");

            if (app_language.equals("Arabic")) {
                holder.product_name.setText(model.getProduct_name_arabic());
            } else {
                holder.product_name.setText(model.getProduct_name());
            }


            // plus click
            holder.plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int quantity = Integer.parseInt(holder.qty.getText().toString());
                    if (quantity >= Integer.parseInt(model.getStock())) {
                        Toast.makeText(con, "Stock limit reached", Toast.LENGTH_SHORT).show();
                    } else {
                        int q = quantity + 1;
                        String qtys = String.valueOf(q);
                        String cart_id = model.getCart_id();
                        holder.qty.setText(qtys);
                        customer_id = baseClass.getSharedPreferance(CartActivity.this, "UserId", "0");
                        UpdateQty(customer_id, cart_id, qtys, holder.subtotal);
                    }
                }
            });
            // minus click
            holder.minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int quantity = Integer.parseInt(holder.qty.getText().toString());
                    if (quantity == 1) {
                        int q = quantity - 0;
                    } else {
                        int q = quantity - 1;
                        String qtys = String.valueOf(q);
                        String cart_id = model.getCart_id();
                        holder.qty.setText(qtys);
                        customer_id = baseClass.getSharedPreferance(CartActivity.this, "UserId", "0");
                        UpdateQty(customer_id, cart_id, qtys, holder.subtotal);
                    }
                }
            });

            holder.lin_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String cart_id = model.getCart_id();
                    AlertDialog.Builder builder = new AlertDialog.Builder(con);
                    builder.setMessage(R.string.deletequestion);
                    builder.setPositiveButton(R.string.yes,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    Delete(cart_id);


                                }
                            });
                    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();

                }
            });

        }

        @Override
        public int getItemCount() {
            return productList.size();
        }

        //add products
        public void UpdateQty(final String customer_id, final String cart_id, final String qtys, final TextView tv_subtotal) {

            if (NetworkConnection.isNetworkStatusAvialable(CartActivity.this)) {

                Api api = BaseClass.getApi();
                Call<ResponseBody> call = api.updateCartData(customer_id, cart_id, qtys);
                call.enqueue(new retrofit2.Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                        if (response.isSuccessful()) {

                            String s = null;
                            try {
                                s = response.body().string();
                                System.out.println("Response.........." + s);
                                JSONObject jo = new JSONObject(s);

                                String message = jo.getString("message");
                                if (message.equals("success")) {

                                    total = jo.getString("total");
                                    String subtotal = jo.getString("subtotal");
                                    txt_total.setText("AED" + " " + total);
                                    tv_subtotal.setText(subtotal);
                                }


                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {

                            Toast.makeText(CartActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        Toast.makeText(CartActivity.this, "Connection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                Toast.makeText(con, "Check your connection", Toast.LENGTH_SHORT).show();
            }


        }

        //delete products
        public void Delete(final String cart_id) {
            if (NetworkConnection.isNetworkStatusAvialable(CartActivity.this)) {
                Api api = BaseClass.getApi();
                Call<ResponseBody> call = api.deleteCartData(cart_id);
                call.enqueue(new retrofit2.Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                        if (response.isSuccessful()) {

                            String s = null;
                            try {
                                s = response.body().string();
                                System.out.println("Response.........." + s);
                                JSONObject jsonObject = new JSONObject(s);

                                String message = jsonObject.getString("message");
                                if (message.equals("success")) {

                                    Toast.makeText(con, "Deleted successfully", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(CartActivity.this, CartActivity.class));
                                    finish();
                                }


                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {

                            Toast.makeText(CartActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        Toast.makeText(CartActivity.this, "Connection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });


            }else {
                Toast.makeText(con, "Check your connection", Toast.LENGTH_SHORT).show();
            }
        }
    }
    }

