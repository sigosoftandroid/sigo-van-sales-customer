package com.vansale.customer.Activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.vansale.customer.Adapter.DashboardAdapter;
import com.vansale.customer.BaseClass;
import com.vansale.customer.Fragment.HomeFragment;
import com.vansale.customer.MenuView;
import com.vansale.customer.R;
import com.vansale.customer.views.DuoDrawerLayout;
import com.vansale.customer.views.DuoMenuView;
import com.vansale.customer.widget.DuoDrawerToggle;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;


public class DashboardActivity extends AppCompatActivity implements MenuView.OnMenuClickListener {

    private DashboardAdapter dashboardAdapter;
    private ViewHolder mViewHolder;
    BaseClass baseClass = new BaseClass();

    TextView txt_name, txt_email, txt_mobile;
    CircleImageView img_profile;
    LinearLayout ll_circle_image;

    RadioGroup radioGroup;
    RadioButton radioButtonEnglish, radioButtonArabic;
    boolean isSlectedLanguageRTL = true;

    private ArrayList<String> mTitles = new ArrayList<>();
    private ArrayList<Integer> images = new ArrayList();

    String name, email, mobileno, image;
    String app_language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(DashboardActivity.this);
        setContentView(R.layout.activity_dashboard);

        txt_name = findViewById(R.id.txt_name);
        txt_email = findViewById(R.id.txt_email);
        txt_mobile = findViewById(R.id.txt_mobileno);
        img_profile = findViewById(R.id.img_profile);
        ll_circle_image = findViewById(R.id.ll_circle_image);
        ll_circle_image.setLayerType(ll_circle_image.LAYER_TYPE_SOFTWARE, null);


        // Language selection

        radioGroup = (RadioGroup) findViewById(R.id.radiogroup);
        radioButtonArabic = (RadioButton) findViewById(R.id.rb_arabic);
        radioButtonEnglish = (RadioButton) findViewById(R.id.rb_english);

        app_language = baseClass.getSharedPreferance(getApplicationContext(), "selectedLanguage", "English");
        if (app_language.equals("Arabic")) {
            radioButtonArabic.setChecked(true);
            radioButtonEnglish.setChecked(false);
            radioButtonArabic.setTextColor(getResources().getColor(R.color.blue));
            radioButtonEnglish.setTextColor(getResources().getColor(R.color.white));
            ChangeLanguage();
        } else {
            radioButtonEnglish.setChecked(true);
            radioButtonArabic.setChecked(false);
            radioButtonArabic.setTextColor(getResources().getColor(R.color.white));
            radioButtonEnglish.setTextColor(getResources().getColor(R.color.blue));
            ChangeLanguage();
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                RadioButton rb = (RadioButton) findViewById(checkedId);
                rb.setTextColor(getResources().getColor(R.color.white));
                baseClass.setSharedPreferance(getApplicationContext(), "selectedLanguage", rb.getText().toString());
                // setting the entire app configuration whether it is arabic or english
                app_language = baseClass.getSharedPreferance(getApplicationContext(), "selectedLanguage", "English");
                ChangeLanguage();
                startActivity(new Intent(DashboardActivity.this, DashboardActivity.class));
                finish();

            }
        });


        name = baseClass.getSharedPreferance(DashboardActivity.this, "Name", "");
        email = baseClass.getSharedPreferance(DashboardActivity.this, "Email", "");
        mobileno = baseClass.getSharedPreferance(DashboardActivity.this, "Mobile", "");
        image = baseClass.getSharedPreferance(DashboardActivity.this, "Image", "");

        txt_name.setText(name);
        txt_email.setText(email);
        txt_mobile.setText(mobileno);


        if (!image.equals("")) {
            Picasso.with(DashboardActivity.this).load(image).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(img_profile);
        }

        mTitles = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.menuOptions)));

        this.images = new ArrayList();

        this.images.add(R.drawable.home1);

        this.images.add(R.drawable.profile);

        this.images.add(R.drawable.myorders);

        this.images.add(R.drawable.coupons);

        this.images.add(R.drawable.contact);

        this.images.add(R.drawable.privacy);

        this.images.add(R.drawable.terms);

        this.images.add(R.drawable.logout);

        // Initialize the views
        mViewHolder = new ViewHolder();

        // Handle toolbar actions
        handleToolbar();

        // Handle menu actions
        handleMenu();

        // Handle drawer actions
        handleDrawer();

        // Show main fragment in container
        goToFragment(new HomeFragment(), false);
        dashboardAdapter.setViewSelected(0, true);
        setTitle(mTitles.get(0));

    }

    private void handleToolbar() {
        setSupportActionBar(mViewHolder.mToolbar);
    }

    private void handleDrawer() {
        DuoDrawerToggle duoDrawerToggle = new DuoDrawerToggle(this,
                mViewHolder.mDuoDrawerLayout,
                mViewHolder.mToolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        mViewHolder.mDuoDrawerLayout.setDrawerListener(duoDrawerToggle);
        duoDrawerToggle.syncState();

    }

   /* private void handleMenu() {
        dashboardAdapter = new DashboardAdapter(DashboardActivity.this,mTitles,images);

        mViewHolder.mDuoMenuView.setOnMenuClickListener(DashboardActivity.this);
        mViewHolder.mDuoMenuView.setAdapter(dashboardAdapter);
    }*/

    private void handleMenu() {

        dashboardAdapter = new DashboardAdapter(DashboardActivity.this, mTitles, images);
        mViewHolder.mDuoMenuView.setOnMenuClickListener(new DuoMenuView.OnMenuClickListener() {
            @Override
            public void onFooterClicked() {

            }

            @Override
            public void onHeaderClicked() {

            }

            @Override
            public void onOptionClicked(int position, Object objectClicked) {
                dashboardAdapter.setViewSelected(position, true);
                switch (position) {
                    case 0:
                        goToFragment(new HomeFragment(), false);
                        break;
                    case 1:
                        startActivity(new Intent(DashboardActivity.this, MyProfileActivity.class));
                        break;
                    case 2:
                        startActivity(new Intent(DashboardActivity.this, MyOrdersActivity.class));
                        break;
                    case 3:
                        startActivity(new Intent(DashboardActivity.this, CouponsActivity.class));
                        break;

                        case 4:
                        startActivity(new Intent(DashboardActivity.this, ContactActivity.class));
                        break;
                    case 5:
                        startActivity(new Intent(DashboardActivity.this, PrivacyPolicyActivity.class));
                        break;
                    case 6:
                        startActivity(new Intent(DashboardActivity.this, TermsandConditionActivity.class));
                        break;
                    case 7:
                        Alertdialog();
                        break;
                }
                mViewHolder.mDuoDrawerLayout.closeDrawer();
            }
        });
//        mViewHolder.mDuoMenuView.setOnMenuClickListener((DuoMenuView.OnMenuClickListener) this);
        mViewHolder.mDuoMenuView.setAdapter(dashboardAdapter);
    }

    @Override
    public void onFooterClicked() {
        Toast.makeText(this, "onFooterClicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onHeaderClicked() {
        // Toast.makeText(this, "onHeaderClicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onOptionClicked(int position, Object objectClicked) {

        // Set the toolbar title
        setTitle(mTitles.get(position));

        // Set the right options selected
        dashboardAdapter.setViewSelected(position, true);

        // Navigate to the right fragment
        switch (position) {
            case 0:
                goToFragment(new HomeFragment(), false);
                break;

            case 1:
                startActivity(new Intent(DashboardActivity.this, MyProfileActivity.class));
                break;

            case 7:
                Alertdialog();
                break;
        }

        // Close the drawer
        mViewHolder.mDuoDrawerLayout.closeDrawer();
    }

    private class ViewHolder {
        private DuoDrawerLayout mDuoDrawerLayout;
        private DuoMenuView mDuoMenuView;
        private Toolbar mToolbar;

        ViewHolder() {
            mDuoDrawerLayout = (DuoDrawerLayout) findViewById(R.id.drawer);
            mDuoMenuView = (DuoMenuView) mDuoDrawerLayout.getMenuView();
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
        }
    }

    private void goToFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }

        transaction.replace(R.id.container, fragment).commit();
    }

    //color for status bar
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient_toolbar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }


    public void Alertdialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(R.string.logout_qstn);
        alertDialogBuilder.setPositiveButton(R.string.yes,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        baseClass.setSharedPreferance(DashboardActivity.this, "Name", "");
                        baseClass.setSharedPreferance(DashboardActivity.this, "Email", "");
                        baseClass.setSharedPreferance(DashboardActivity.this, "Mobile", "");
                        baseClass.setSharedPreferance(DashboardActivity.this, "Image", "");
                        baseClass.setSharedPreferance(DashboardActivity.this, "UserId", "0");
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                        Toast.makeText(DashboardActivity.this, "Successfully logged out", Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                    }
                });

        alertDialogBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void setLocale(String lang) {
//        Locale myLocale = new Locale(lang);
        Locale myLocale = new Locale(lang,"MA");
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    protected void onResume() {
        super.onResume();
        name = baseClass.getSharedPreferance(DashboardActivity.this, "Name", "");
        email = baseClass.getSharedPreferance(DashboardActivity.this, "Email", "");
        mobileno = baseClass.getSharedPreferance(DashboardActivity.this, "Mobile", "");
        image = baseClass.getSharedPreferance(DashboardActivity.this, "Image", "");
        txt_name.setText(name);
        txt_email.setText(email);
        txt_mobile.setText(mobileno);

        if (!image.equals("")) {
            Picasso.with(DashboardActivity.this).load(image).placeholder(R.drawable.user_default).into(img_profile);
        }

    }


    public void ChangeLanguage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Configuration configuration = getResources().getConfiguration();
            if (app_language.contains("Arabic")) {
                configuration.setLayoutDirection(new Locale("ar"));
                radioButtonArabic.setTextColor(getResources().getColor(R.color.blue));
                radioButtonEnglish.setTextColor(getResources().getColor(R.color.white));
                setLocale("ar");
                isSlectedLanguageRTL = true;

            } else {
                configuration.setLayoutDirection(Locale.ENGLISH);
                radioButtonArabic.setTextColor(getResources().getColor(R.color.white));
                radioButtonEnglish.setTextColor(getResources().getColor(R.color.blue));
                setLocale("en");
                isSlectedLanguageRTL = false;
            }
            getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
        }
    }
}
