package com.vansale.customer.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;

import com.vansale.customer.Retrofit.Api;


import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vansale.customer.BaseClass;
import com.vansale.customer.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Random;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText mobileEditext;
    Button login;
    TextView forgotPswd,toolbarTitle,dontAccount,lblLogin;
    Button btnRegister;
    String mobile;
    Typeface typeface,typeface1,typeface2;
    BaseClass baseClass = new BaseClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(LoginActivity.this);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        mobileEditext = findViewById(R.id.txt_mobile);
        login = findViewById(R.id.btn_submit);
        forgotPswd = findViewById(R.id.txt_forgotPswd);
        btnRegister = findViewById(R.id.btn_register);
        toolbarTitle = findViewById(R.id.toolbar_title);
        dontAccount = findViewById(R.id.txt_dntAccount);
        lblLogin = findViewById(R.id.lbl_login);

        //setting typeface
        typeface = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        toolbarTitle.setTypeface(typeface);

        typeface1 = Typeface.createFromAsset(getAssets(), "fonts/Avenir_LTD_Light.ttf");
        mobileEditext.setTypeface(typeface1);

        btnRegister.setTypeface(typeface1);
        forgotPswd.setTypeface(typeface1);
        dontAccount.setTypeface(typeface1);

        typeface2 = Typeface.createFromAsset(getAssets(), "fonts/Avenir_Black.ttf");
        lblLogin.setTypeface(typeface2);


    }

    //color for status bar
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient_toolbar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    public void loginClick(View view) {

        if (mobileEditext.getText().toString().length() < 9) {

            mobileEditext.setError(getString(R.string.please_enter_valid_phone));
            mobileEditext.requestFocus();
        }
        else {
            mobile = mobileEditext.getText().toString();

            CheckMObile();
        }

    }

    //service for checking mobile number exist
    public void CheckMObile() {
        if(NetworkConnection.isNetworkStatusAvialable(LoginActivity.this)) {
            final ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
            Api api = BaseClass.getApi();
            Call<ResponseBody> call = api.checkMobile(mobile);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                    if (response.isSuccessful()) {
                        dialog.dismiss();

                        String s = null;
                        try {
                            s = response.body().string();
                            System.out.println("Response.........." + s);
                            JSONObject jsonObject = new JSONObject(s);

                            String status = jsonObject.getString("message");

                            if (status.equals("success")) {

                                Random rand = new Random();
                                String myotp = String.format("%04d", rand.nextInt(10000));

                                sendOTP(mobile,myotp);


                            } else if(status.equals("Your account have been blocked")){

                                Toast.makeText(LoginActivity.this,status, Toast.LENGTH_SHORT).show();
                            }
                            else {

                                Toast.makeText(LoginActivity.this, "Mobile number not registered", Toast.LENGTH_SHORT).show();

                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {

                        Toast.makeText(LoginActivity.this, "Response error", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Toast.makeText(LoginActivity.this, "Connection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            });
        }else {
            Toast.makeText(this, "Check your connection", Toast.LENGTH_SHORT).show();
           
        }


    }

    private void sendOTP(String phone, final String myotp) {
        final ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.show();
        Api api = BaseClass.getApi();
        Call<ResponseBody> call = api.sendOtp(phone,myotp);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    dialog.dismiss();
                    try {
                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                        if (jsonRESULTS.getString("message").equals("Success")) {

                            Toast.makeText(LoginActivity.this,"An OTP has been sent to your mobile number successfully", Toast.LENGTH_SHORT).show();
                            Log.e("myotp",myotp);
                            Toast.makeText(LoginActivity.this, myotp, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginActivity.this, OTPActivity.class).putExtra("mobile", mobile).putExtra("otp", myotp);
                            startActivity(intent);

                        } else {
                            Toast.makeText(LoginActivity.this, "OTP Sending Failed", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                } else {
//                            loading.dismiss();
                    Toast.makeText(LoginActivity.this, "Response error", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(LoginActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                dialog.dismiss();

            }
        });
    }


    public void registerClick(View view) {
        startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
        finish();

    }



}
