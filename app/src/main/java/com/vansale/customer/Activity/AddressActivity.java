package com.vansale.customer.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vansale.customer.Retrofit.Api;
import com.vansale.customer.BaseClass;
import com.vansale.customer.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class AddressActivity extends AppCompatActivity {

    TextView toolbarTitle;
    TextView lbl_deliverydate;
    TextView txt_deliverydate;
    TextView lbl_total;
    TextView lbl_deliverytime;
    TextView txt_deliverytime;
    TextView txt_total;
    Button btn_confirm;
    String latitude, longitude, sub_locality, locality, city, pincode;
    BaseClass baseClass = new BaseClass();

    Typeface typeface, typeface1, typeface2, typeface3;
    String customer_id,houseNumber,building;

    String time,accnt_type;
    String total;
    String date;
    TextView txt_line1;
    TextView txt_line2,txt_city;
    EditText txt_housenumber,txt_builing;
    Spinner spinner_account;
    public static String btn_locationchangeClick  = "false";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(AddressActivity.this);
        setContentView(R.layout.activity_address);

        toolbarTitle = findViewById(R.id.toolbar_title);
        txt_line1 =  findViewById(R.id.txt_line1);
        txt_line2 = findViewById(R.id.txt_line2);
        txt_city = findViewById(R.id.txt_city);
        txt_housenumber = findViewById(R.id.txt_housenumber);
        txt_builing = findViewById(R.id.txt_building);
        lbl_deliverydate = findViewById(R.id.lbl_deliverydate);
        txt_deliverydate = findViewById(R.id.txt_deliverydate);
        lbl_total = findViewById(R.id.lbl_total);
        txt_total = findViewById(R.id.txt_total);
        btn_confirm = findViewById(R.id.btn_confirm);
        lbl_deliverytime = findViewById(R.id.lbl_deliverytime);
        txt_deliverytime = findViewById(R.id.txt_deliverytime);
        spinner_account = findViewById(R.id.spin_acc_type);

        getSupportActionBar().hide();


        total = baseClass.getSharedPreferance(AddressActivity.this, "total", "0");
        txt_total.setText(total);

        //setting typeface
        typeface = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        toolbarTitle.setTypeface(typeface);
        typeface1 = Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd_Roman.ttf");
        lbl_deliverydate.setTypeface(typeface1);
        lbl_deliverytime.setTypeface(typeface1);
        lbl_total.setTypeface(typeface1);
        typeface2 = Typeface.createFromAsset(getAssets(), "fonts/Avneir_Medium.ttf");
        txt_deliverydate.setTypeface(typeface2);
        txt_deliverytime.setTypeface(typeface2);
        txt_total.setTypeface(typeface2);
        typeface3 = Typeface.createFromAsset(getAssets(), "fonts/Avenir_LTD_Light.ttf");
        btn_confirm.setTypeface(typeface3);

        customer_id = baseClass.getSharedPreferance(AddressActivity.this, "UserId", "0");
        getCustomerAddress(customer_id);


        spinner_account.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                accnt_type = spinner_account.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        // setting delivery date and delivery time
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        Date ct = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(ct);

        if (timeOfDay >= 0 && timeOfDay < 12) {

            txt_deliverytime.setText("5:00 pm");
            txt_deliverydate.setText(formattedDate);

        } else {

            txt_deliverytime.setText("11:00 am");
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 1);
            String nextdate = df.format(calendar.getTime());
            txt_deliverydate.setText(nextdate);
        }

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                time = txt_deliverytime.getText().toString();

                date = txt_deliverydate.getText().toString();
                time = txt_deliverytime.getText().toString();

                houseNumber = txt_housenumber.getText().toString();
                building = txt_builing.getText().toString();
                sub_locality = txt_line1.getText().toString();
                locality = txt_line2.getText().toString();
                city = txt_city.getText().toString();

                    Date initDate = null;
                    try {
                        initDate = new SimpleDateFormat("dd-MMM-yyyy").parse(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String parsedDate = formatter.format(initDate);

                    if(houseNumber.length()==0){
                        Toast.makeText(AddressActivity.this, "Please enter your house number", Toast.LENGTH_SHORT).show();
                    }else if(building.length()==0) {
                        Toast.makeText(AddressActivity.this, "Please enter your building", Toast.LENGTH_SHORT).show();
                    }else if(sub_locality.length()==0) {
                        Toast.makeText(AddressActivity.this, "Please enter your sub locality", Toast.LENGTH_SHORT).show();
                    }else if (accnt_type.equals("Account Type")) {

                        Toast.makeText(AddressActivity.this, R.string.choose_account_type, Toast.LENGTH_SHORT).show();

                    } else {

                        baseClass.setSharedPreferance(AddressActivity.this, "building", building);
                        baseClass.setSharedPreferance(AddressActivity.this, "housenumber", houseNumber);
                        baseClass.setSharedPreferance(AddressActivity.this, "sublocality", sub_locality);
                        baseClass.setSharedPreferance(AddressActivity.this, "locality", locality);
                        baseClass.setSharedPreferance(AddressActivity.this, "city", city);

                        String delivery_date = parseDateToddMMyyyy(date);
//                       Log.e("loc1",sub_locality);
//                        Log.e("loc2",locality);
//                        Log.e("loc3",city);

                        OrderPlace(customer_id,delivery_date,time,building,houseNumber,String.format("%6f",Double.valueOf(latitude)),String.format("%6f",Double.valueOf(longitude)),sub_locality+","+locality+","+city,accnt_type);
                    }
            }
        });
    }

    public void backClick(View view) {

        finish();
    }

    // set status bar
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient_toolbar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    private void getCustomerAddress(String customer_id) {
        Api api = BaseClass.getApi();
        Call<ResponseBody> call = api.getAddress(customer_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    String s = null;
                    try {
                        s = response.body().string();
                        System.out.println("address.........." + s);
                        JSONObject jo = new JSONObject(s);

                        String message = jo.getString("message");
                        if (message.equals("Success")) {
                            JSONObject address = jo.getJSONObject("address");
                            String house = address.getString("house");
                            String building = address.getString("building");
                            String location = address.getString("location");
                            latitude = address.getString("latitude");
                            longitude = address.getString("longitude");


                            String parts[] = location.split(",");

                            txt_housenumber.setText(house);
                            txt_builing.setText(building);

                            if(parts[0].equals("")){
                                txt_line1.setText(location);
                            }else {
                                txt_line1.setText(parts[0]);
                            }
//                            if(parts[1].equals("")){
//                                txt_line2.setText(location);
//                            }else {
//                                txt_line2.setText(parts[1]);
//                            }
//                            if(parts[2].equals("")){
//                                txt_city.setText(location);
//                            }else {
//                                txt_city.setText(parts[2]);
//                            }

                        }else {
//                            Toast.makeText(AddressActivity.this, "Address not available", Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(AddressActivity.this, "NetworkConnection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }


    public void addAddressClick(View view) {
        startActivity(new Intent(AddressActivity.this, MapsActivity.class));
//        finish();
    }

    public void OrderPlace(String customer_id, String delivery_date, String time, String building, String houseNumber, String latitude, String longitude, String location, String accnt_type) {
        if(NetworkConnection.isNetworkStatusAvialable(AddressActivity.this)) {
            final ProgressDialog dialog = new ProgressDialog(AddressActivity.this);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
            Api api = BaseClass.getApi();
            Call<ResponseBody> call = api.placeOrder(customer_id, delivery_date, time, building, houseNumber, latitude, longitude, location, accnt_type);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                    if (response.isSuccessful()) {
                        dialog.dismiss();

                        String s = null;
                        try {
                            s = response.body().string();
                            System.out.println("Response.........." + s);
                            JSONObject jsonObject = new JSONObject(s);

                            String message = jsonObject.getString("message");
                            if (message.equals("success")) {

                                Toast.makeText(AddressActivity.this, R.string.order_placed_success, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(AddressActivity.this, OrderConfirmationActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();

                            } else {
                                Toast.makeText(AddressActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                                Log.e("errorbody", response.body().string());
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {

                        Toast.makeText(AddressActivity.this, "Response error", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Toast.makeText(AddressActivity.this, "Connection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            });
        }else {
            Toast.makeText(this, "Check your connection", Toast.LENGTH_SHORT).show();

        }


    }


    @Override
    protected void onRestart() {
        super.onRestart();

        if(btn_locationchangeClick.equals("true")) {

            latitude = baseClass.getSharedPreferance(AddressActivity.this, "latitude", latitude);
            longitude = baseClass.getSharedPreferance(AddressActivity.this, "longitude", longitude);
            sub_locality = baseClass.getSharedPreferance(AddressActivity.this, "sub_locality", sub_locality);
            locality = baseClass.getSharedPreferance(AddressActivity.this, "locality", locality);
            city = baseClass.getSharedPreferance(AddressActivity.this, "city", city);
            pincode = baseClass.getSharedPreferance(AddressActivity.this, "pincode", pincode);

            if (!sub_locality.equals("null") || (sub_locality != null)) {
                txt_line1.setText(sub_locality);
            }
            if (!locality.equals("null") || (locality != null)) {
                txt_line2.setText(locality);
            }
            if (!city.equals("null") || (city != null) || (!city.equals(""))) {
                txt_city.setText(city);
            }
        }else {

            getCustomerAddress(customer_id);
        }
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "dd-MMM-yyyy";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}
