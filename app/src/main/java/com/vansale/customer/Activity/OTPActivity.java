package com.vansale.customer.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.vansale.customer.BaseClass;
import com.vansale.customer.R;
import com.vansale.customer.Retrofit.Api;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;

public class OTPActivity extends AppCompatActivity {

    EditText editText1, editText2, editText3, editText4;
    String myotp, mobile;
    BaseClass baseClass = new BaseClass();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(OTPActivity.this);
        setContentView(R.layout.activity_otp);
        getSupportActionBar().hide();

        Intent intent = getIntent();
        myotp = intent.getStringExtra("otp");
        mobile = intent.getStringExtra("mobile");

//        Toast.makeText(this, myotp, Toast.LENGTH_SHORT).show();

        editText1 = findViewById(R.id.editext);
        editText2 = findViewById(R.id.editext1);
        editText3 = findViewById(R.id.editext2);
        editText4 = findViewById(R.id.editext3);


        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (editText1.getText().toString().trim().length() == 1) {
                    editText1.clearFocus();
                    editText2.requestFocus();
                }
            }
        });

        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (editText2.getText().toString().trim().length() == 1) {
                    editText2.clearFocus();
                    editText3.requestFocus();
                } else if (editText2.getText().toString().trim().length() == 0) {

                    editText2.clearFocus();
                    editText1.requestFocus();
                }
            }
        });
        editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (editText3.getText().toString().trim().length() == 1) {
                    editText3.clearFocus();
                    editText4.requestFocus();
                } else if (editText3.getText().toString().trim().length() == 0) {
                    editText3.clearFocus();
                    editText2.requestFocus();
                }
            }
        });

        editText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (editText4.getText().toString().trim().length() == 0) {
                    editText4.clearFocus();
                    editText3.requestFocus();
                }

                String otp1 = editText1.getText().toString();
                String otp2 = editText2.getText().toString();
                String otp3 = editText3.getText().toString();
                String otp4 = editText4.getText().toString();
                String otp = otp1 + otp2 + otp3 + otp4;

                if (otp1.equals("")) {
                    Toast.makeText(OTPActivity.this, "Invalid OTP", Toast.LENGTH_SHORT).show();
                } else if (otp2.equals("")) {
                    Toast.makeText(OTPActivity.this, "Invalid OTP", Toast.LENGTH_SHORT).show();
                } else if (otp3.equals("")) {
                    Toast.makeText(OTPActivity.this, "Invalid OTP", Toast.LENGTH_SHORT).show();
                } else if (otp4.equals("")) {
                    Toast.makeText(OTPActivity.this, "Invalid OTP", Toast.LENGTH_SHORT).show();
                } else if ((!otp1.equals("")) && (!otp2.equals("")) && (!otp3.equals("")) && (!otp4.equals("")) && myotp.equals(otp)) {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editText4.getWindowToken(), 0);
                   Login(mobile);

                }else {
                    Toast.makeText(OTPActivity.this, "Invalid OTP", Toast.LENGTH_SHORT).show();
                }


            }
        });


    }

    //service for login
    public void Login(String mobile){
        if(NetworkConnection.isNetworkStatusAvialable(OTPActivity.this)) {
            Api api = BaseClass.getApi();
            Call<ResponseBody> call = api.login(mobile);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                    if (response.isSuccessful()) {

                        String s = null;
                        try {
                            s = response.body().string();
                            System.out.println("Response.........." + s);
                            JSONObject jo = new JSONObject(s);

                            String status = jo.getString("message");
                            if (status.equals("success")) {
                                String user_id = jo.getString("user_id");
                                String name = jo.getString("name");
                                String email = jo.getString("email");
                                String mobile = jo.getString("mobile");
                                String address = jo.getString("address");
                                String trn = jo.getString("trn");
                                String profile = BaseClass.mainURL + jo.getString("image");
                                Toast.makeText(OTPActivity.this, "Login Success", Toast.LENGTH_SHORT).show();
                                baseClass.setSharedPreferance(getApplicationContext(), "UserId", user_id);
                                baseClass.setSharedPreferance(getApplicationContext(), "Email", email);
                                baseClass.setSharedPreferance(getApplicationContext(), "Mobile", mobile);
                                baseClass.setSharedPreferance(getApplicationContext(), "Name", name);
                                baseClass.setSharedPreferance(getApplicationContext(), "Address", address);
                                baseClass.setSharedPreferance(getApplicationContext(), "trn", trn);
                                baseClass.setSharedPreferance(getApplicationContext(), "Image", profile);
                                Intent intent = new Intent(OTPActivity.this, DashboardActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                        FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();

                            } else if (status.equals("failed")) {
                                Toast.makeText(OTPActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
                            }


                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {

                        Toast.makeText(OTPActivity.this, "Response error", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Toast.makeText(OTPActivity.this, "Connection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }else {
            Toast.makeText(this, "Check your connection", Toast.LENGTH_SHORT).show();
        }

    }
    private void sendOTP(String phone, final String myotp) {
        final ProgressDialog dialog = new ProgressDialog(OTPActivity.this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.show();
        Api api = BaseClass.getApi();
        Call<ResponseBody> call = api.sendOtp(phone,myotp);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    dialog.dismiss();
                    try {
                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                        if (jsonRESULTS.getString("message").equals("Success")) {

                            Toast.makeText(OTPActivity.this,"An OTP has been sent to your mobile number successfully", Toast.LENGTH_SHORT).show();
                            Log.e("myotp",myotp);
                            Toast.makeText(OTPActivity.this, myotp, Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(OTPActivity.this, OTPActivity.class).putExtra("mobile", mobile).putExtra("otp", myotp);
                            startActivity(intent);

                        } else {
                            Toast.makeText(OTPActivity.this, "OTP Sending Failed", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                } else {
//                            loading.dismiss();
                    Toast.makeText(OTPActivity.this, "Response error", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(OTPActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                dialog.dismiss();

            }
        });
    }


    public void reSendOTP(View view) {
//        Toast.makeText(this, myotp, Toast.LENGTH_SHORT).show();
        sendOTP(mobile, myotp);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    //color for status bar
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient_toolbar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

}
