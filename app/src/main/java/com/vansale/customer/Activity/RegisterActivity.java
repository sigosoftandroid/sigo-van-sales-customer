package com.vansale.customer.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;

import androidx.annotation.NonNull;

import com.google.android.material.textfield.TextInputEditText;

import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import com.vansale.customer.BaseClass;
import com.vansale.customer.R;
import com.vansale.customer.Retrofit.Api;
import com.yalantis.ucrop.UCrop;
//import com.theartofdev.edmodo.cropper.CropImage;
//import com.theartofdev.edmodo.cropper.CropImageView;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegisterActivity extends AppCompatActivity {

    EditText nameLatinEdittext, emailEdittext, phoneEdittext,addressEdittext,trnNimberEdittext;
    Button btn_register, btnLogin;
    TextView txtAlreadyAccount, txtRegister, lblRegister;
    Typeface typeface, typeface1, typeface2;
    String name_latin, email, phone, password,address,trnNumber;
    String  myotp;
    CircleImageView iv_profile;
    BaseClass baseClass = new BaseClass();
    TextInputEditText passwordEdittext;
    /**
     * Persist URI image to crop URI if specific permissions are required
     */
    private Uri mCropImageUri;
    Bitmap croped_img;
    Bitmap converetdImage;
    String image1converted = "";

    private Uri cameraImageUri;
    private static final int REQUEST_CAMERA = 1;
    private static final int REQUEST_GALLERY = 2;
    private static final int REQUEST_CROP = 3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(RegisterActivity.this);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();

        nameLatinEdittext = findViewById(R.id.txt_name_latin);
        emailEdittext = findViewById(R.id.txt_email);
        phoneEdittext = findViewById(R.id.txt_phone);
        passwordEdittext = findViewById(R.id.txt_password);
        addressEdittext = findViewById(R.id.txt_address);

        txtRegister = findViewById(R.id.toolbar_title);
        lblRegister = findViewById(R.id.lbl_register);
        txtAlreadyAccount = findViewById(R.id.txt_alreadyAccount);
        trnNimberEdittext = findViewById(R.id.txt_trn);


        btn_register = findViewById(R.id.btn_register);
        btnLogin = findViewById(R.id.btn_login);

        iv_profile = findViewById(R.id.iv_profile_pic);

        //setting typeface
        typeface = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        txtRegister.setTypeface(typeface);

        typeface1 = Typeface.createFromAsset(getAssets(), "fonts/Avenir_LTD_Light.ttf");
        nameLatinEdittext.setTypeface(typeface1);
        emailEdittext.setTypeface(typeface1);
        phoneEdittext.setTypeface(typeface1);
        addressEdittext.setTypeface(typeface1);
        passwordEdittext.setTypeface(typeface1);
        btn_register.setTypeface(typeface1);
        btnLogin.setTypeface(typeface1);
        txtAlreadyAccount.setTypeface(typeface1);

        typeface2 = Typeface.createFromAsset(getAssets(), "fonts/Avenir_Black.ttf");
        lblRegister.setTypeface(typeface2);

        // Profile image click
        iv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (ActivityCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.CAMERA)
//                        != PackageManager.PERMISSION_GRANTED) {
//                    // Camera permission has not been granted.
//                    requestCameraPermission();
//
//                } else {
                openGallery();
//                    onSelectImageClick(view);
//                }

            }
        });



        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                finish();
            }
        });

    }

    //color for status bar
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient_toolbar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }


    public void registerClick(View view) {

        email = emailEdittext.getText().toString();


        if (isEmpty(nameLatinEdittext)) {

            nameLatinEdittext.setError(getString(R.string.please_enter_name));
            nameLatinEdittext.requestFocus();

        }
       /* else if (isEmpty(nameArabicEdittext)) {

            nameArabicEdittext.setError("Please enter your name in Arabic");
            nameArabicEdittext.requestFocus();

        } */

        else if (phoneEdittext.getText().toString().length() < 9) {

            phoneEdittext.setError(getString(R.string.please_enter_valid_phone));
            phoneEdittext.requestFocus();
        } else if ((email.length()!=0)&&(!isValidEmail(email))) {

            emailEdittext.setError(getString(R.string.please_enter_valid_email));
            emailEdittext.requestFocus();

        }else if (isEmpty(addressEdittext)) {

            addressEdittext.setError(getString(R.string.enteraddress));
            addressEdittext.requestFocus();
        }

        else if (passwordEdittext.getText().toString().length() < 8 && !isValidPassword(passwordEdittext.getText().toString())) {

//            passwordEdittext.setError(getString(R.string.password_minimum));
//            passwordEdittext.requestFocus();
            Toast.makeText(this, getString(R.string.password_minimum), Toast.LENGTH_SHORT).show();

        }
//        else if (converetdImage == null) {
//
//            Toast.makeText(RegisterActivity.this, R.string.choose_profile_image, Toast.LENGTH_SHORT).show();
//
//        }
        else {
            if (converetdImage != null) {

                image1converted = getStringImage(converetdImage);
            }

            name_latin = nameLatinEdittext.getText().toString();
            email = emailEdittext.getText().toString();
            phone = phoneEdittext.getText().toString();
            address = addressEdittext.getText().toString();
            trnNumber = trnNimberEdittext.getText().toString();


            password = passwordEdittext.getText().toString();

//            Random rand = new Random();
//            myotp = String.format("%04d", rand.nextInt(10000));

            CheckMObile();
        }

    }

    // validation for Password
    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();
    }

    // validation for email
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    // validation for name
    boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    public void loginClick(View view) {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        finish();
    }


//    private void requestCameraPermission() {
//        //   Toast.makeText(EditProfileActivity.this, "CAMERA permission has NOT been granted. Requesting permission.", Toast.LENGTH_SHORT).show();
//        if (ActivityCompat.shouldShowRequestPermissionRationale(RegisterActivity.this,
//                Manifest.permission.CAMERA)) {
//            // Provide an additional rationale to the user if the permission was not granted
//            // and the user would benefit from additional context for the use of the permission.
//            // For example if the user has previously denied the permission.
//            //  Toast.makeText(EditProfileActivity.this, "Displaying camera permission rationale to provide additional context.", Toast.LENGTH_SHORT).show();
//            ActivityCompat.requestPermissions(RegisterActivity.this,
//                    new String[]{Manifest.permission.CAMERA},
//                    13);
//
//        } else {
//
//            // Camera permission has not been granted yet. Request it directly.
//            ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.CAMERA},
//                    13);
//
//        }
//        // END_INCLUDE(camera_permission_request)
//    }
//
//    public void onSelectImageClick(View view) {
//        CropImage.startPickImageActivity(this);
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//
//        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            // required permissions granted, start crop image activity
//            startCropImageActivity(mCropImageUri);
//        } else {
//            // startCropImageActivity(mCropImageUri);
//            CropImage.startPickImageActivity(this);
//            // Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
//        }
//
//    }
//
//    /**
//     * Start crop image activity for the given image.
//     */
//    private void startCropImageActivity(Uri imageUri) {
//        CropImage.activity(imageUri)
//                .setGuidelines(CropImageView.Guidelines.ON)
//                .setMultiTouchEnabled(true)
//                .start(this);
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_CANCELED) {
//            return;
//        }
//
//        // handle result of pick image chooser
//        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
//            Uri imageUri = CropImage.getPickImageResultUri(this, data);
//
//            // For API >= 23 we need to check specifically that we have permissions to read external storage.
//            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
//                // request permissions and handle the result in onRequestPermissionsResult()
//                mCropImageUri = imageUri;
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
//                }
//            } else {
//                // no permissions required or already grunted, can start crop image activity
//                startCropImageActivity(imageUri);
//            }
//        }
//        // handle result of CropImageActivity
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
//                iv_profile.setImageURI(result.getUri());
//
//                try {
//                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), result.getUri());
//                    croped_img = bitmap;
//                    if (croped_img != null) {
//
//                        converetdImage = getResizedBitmap(bitmap, 200);
//
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
////                Toast.makeText(this, "Cropping successful, Sample: " + croped_img, Toast.LENGTH_LONG).show();
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
////                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
//            }
//        }
//
//    }

    private void requestPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            androidx.core.app.ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, 100);
        }
    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            if (photoFile != null) {
                cameraImageUri = FileProvider.getUriForFile(this, getPackageName() + ".fileprovider", photoFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri);
                startActivityForResult(cameraIntent, REQUEST_CAMERA);
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(null);
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    private void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, REQUEST_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    startCrop(cameraImageUri);
                    break;
                case REQUEST_GALLERY:
                    if (data != null && data.getData() != null) {
                        startCrop(data.getData());
                    }
                    break;
                case UCrop.REQUEST_CROP:
                    if (data != null) {
                        Uri resultUri = UCrop.getOutput(data);
                        if (resultUri != null) {
                            iv_profile.setImageURI(resultUri);

                            try {
                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                                croped_img = bitmap;
                                if (croped_img != null) {

                                    converetdImage = getResizedBitmap(bitmap, 200);

                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;
                case UCrop.RESULT_ERROR:
                    Throwable cropError = UCrop.getError(data);
                    if (cropError != null) {
                        Toast.makeText(this, cropError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    break;
            }

        }
    }

    private void startCrop(Uri sourceUri) {
        Uri destinationUri = Uri.fromFile(new File(getCacheDir(), "croppedImage.jpg"));
        UCrop.of(sourceUri, destinationUri)
                .withAspectRatio(1, 1)
                .withMaxResultSize(800, 800)
                .start(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @androidx.annotation.NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permissions granted
            } else {
                Toast.makeText(this, "Permissions denied", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void requestCameraPermission() {
        //   Toast.makeText(EditProfileActivity.this, "CAMERA permission has NOT been granted. Requesting permission.", Toast.LENGTH_SHORT).show();
        if (ActivityCompat.shouldShowRequestPermissionRationale(RegisterActivity.this,
                Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            //  Toast.makeText(EditProfileActivity.this, "Displaying camera permission rationale to provide additional context.", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(RegisterActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    13);

        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.CAMERA},
                    13);

        }
        // END_INCLUDE(camera_permission_request)
    }


    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public String getStringImage(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public void ShowDialog() {
        final MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .customView(R.layout.dialog_otp, true)
                .cancelable(true)
                .autoDismiss(false);
        dialog = builder.build();
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final EditText editText1 = (EditText) dialog.findViewById(R.id.editext);
        final EditText editText2 = (EditText) dialog.findViewById(R.id.editext1);
        final EditText editText3 = (EditText) dialog.findViewById(R.id.editext2);
        final EditText editText4 = (EditText) dialog.findViewById(R.id.editext3);
        final Button submit = (Button) dialog.findViewById(R.id.btn_otp);


        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (editText1.getText().toString().trim().length() == 1) {
                    editText1.clearFocus();
                    editText2.requestFocus();
                }
            }
        });

        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (editText1.getText().toString().trim().length() == 1) {
                    editText2.clearFocus();
                    editText3.requestFocus();
                }
            }
        });
        editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (editText1.getText().toString().trim().length() == 1) {
                    editText3.clearFocus();
                    editText4.requestFocus();
                }
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String otp1 = editText1.getText().toString();
                String otp2 = editText2.getText().toString();
                String otp3 = editText3.getText().toString();
                String otp4 = editText4.getText().toString();
                String otp = otp1 + otp2 + otp3 + otp4;
                if (myotp.equals(otp)) {
                    Register();
                } else {
                    Toast.makeText(RegisterActivity.this, R.string.invalid_otp, Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    private void sendOTP(String phone, final String myotp) {
        final ProgressDialog dialog = new ProgressDialog(RegisterActivity.this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.show();
        Api api = BaseClass.getApi();
        Call<ResponseBody> call = api.sendOtp(phone,myotp);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    dialog.dismiss();
                    try {
                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                        if (jsonRESULTS.getString("message").equals("Success")) {

                            Toast.makeText(RegisterActivity.this,"An OTP has been sent to your mobile number successfully", Toast.LENGTH_SHORT).show();
                            Log.e("myotp",myotp);
                            Toast.makeText(RegisterActivity.this, myotp, Toast.LENGTH_SHORT).show();
                            ShowDialog();

                        } else {
                            Toast.makeText(RegisterActivity.this, "OTP Sending Failed", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                } else {
//                            loading.dismiss();
                    Toast.makeText(RegisterActivity.this, "Response error", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(RegisterActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                dialog.dismiss();

            }
        });
    }



    //service for checking mobile number exist
    public void CheckMObile() {

        if(NetworkConnection.isNetworkStatusAvialable(RegisterActivity.this)) {
            Api api = BaseClass.getApi();
            Call<ResponseBody> call = api.checkMobile(phone);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                    if (response.isSuccessful()) {

                        String s = null;
                        try {
                            s = response.body().string();
                            System.out.println("Response.........." + s);
                            JSONObject jsonObject = new JSONObject(s);

                            String status = jsonObject.getString("message");
                            if (status.equals("success")) {

                                Toast.makeText(RegisterActivity.this, R.string.mobile_number_already, Toast.LENGTH_SHORT).show();

                            } else {


                                Random rand = new Random();
                                myotp = String.format("%04d", rand.nextInt(10000));

                                sendOTP(phone,myotp);
//                                Toast.makeText(RegisterActivity.this, myotp, Toast.LENGTH_LONG).show();


                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {

                        Toast.makeText(RegisterActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Toast.makeText(RegisterActivity.this, "Connection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }else {
            Toast.makeText(RegisterActivity.this, "Check your connection", Toast.LENGTH_LONG).show();
        }


    }

    public void Register() {
        if (NetworkConnection.isNetworkStatusAvialable(RegisterActivity.this)) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage(getString(R.string.loading_dialog));
            dialog.setCancelable(false);
            dialog.show();
            Api api = BaseClass.getApi();
            Call<ResponseBody> call = api.register(name_latin, phone, email, password, image1converted, address,trnNumber);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                    if (response.isSuccessful()) {
                        dialog.dismiss();
                        String s = null;
                        try {
                            s = response.body().string();
                            System.out.println("Response.........." + s);
                            JSONObject jo = new JSONObject(s);

                            String status = jo.getString("message");
                            if (status.equals("success")) {
                                JSONObject data = jo.getJSONObject("data");
                                String user_id = data.getString("user_id");
                                String name = data.getString("name");
                                String email = data.getString("email");
                                String mobile = data.getString("phone");
                                String address = data.getString("address");
                                String trn = data.getString("trn");
                                String profile = BaseClass.mainURL + data.getString("image");
                                baseClass.setSharedPreferance(getApplicationContext(), "UserId", user_id);
                                baseClass.setSharedPreferance(getApplicationContext(), "Email", email);
                                baseClass.setSharedPreferance(getApplicationContext(), "Mobile", mobile);
                                baseClass.setSharedPreferance(getApplicationContext(), "Address", address);
                                baseClass.setSharedPreferance(getApplicationContext(), "Name", name);
                                baseClass.setSharedPreferance(getApplicationContext(), "Image", profile);
                                baseClass.setSharedPreferance(getApplicationContext(), "trn", trn);

                                Toast.makeText(RegisterActivity.this, R.string.register_success, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            } else if (status.equals("failed")) {
                                String mobile = jo.getString("mobile");
                                String email = jo.getString("email");
                                if (email.equals("false")) {
                                    Toast.makeText(RegisterActivity.this, R.string.user_already_registered, Toast.LENGTH_SHORT).show();
                                } else if ((mobile.equals("false"))) {
                                    Toast.makeText(RegisterActivity.this, R.string.user_already_registered, Toast.LENGTH_SHORT).show();
                                }

                            }


                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {

                        Toast.makeText(RegisterActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Toast.makeText(RegisterActivity.this, "Connection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            });


        }else {
            Toast.makeText(this, "Check your connection", Toast.LENGTH_SHORT).show();
        }
    }

}






