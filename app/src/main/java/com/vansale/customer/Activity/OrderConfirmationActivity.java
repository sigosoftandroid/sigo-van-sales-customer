package com.vansale.customer.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.vansale.customer.BaseClass;
import com.vansale.customer.R;


public class OrderConfirmationActivity extends AppCompatActivity {
    TextView txt_house, txt_road, txt_building, txt_sublocality, txt_locality, txt_total_price,txt_city;
    String total;
    String building,housenumber,sublocality,locality,city;
    String subarea;
    BaseClass baseClass = new BaseClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(OrderConfirmationActivity.this);
        setContentView(R.layout.activity_order_confirmation);
        getSupportActionBar().hide();

        txt_house = (TextView) findViewById(R.id.txt_housenumber);
        txt_building = (TextView) findViewById(R.id.txt_building);
        txt_sublocality = (TextView) findViewById(R.id.txt_sublocality);
        txt_locality = (TextView) findViewById(R.id.txt_locality);
        txt_city = (TextView) findViewById(R.id.txt_city);
        txt_total_price = (TextView) findViewById(R.id.txt_total);


        building = baseClass.getSharedPreferance(OrderConfirmationActivity.this, "building", "");
        housenumber = baseClass.getSharedPreferance(OrderConfirmationActivity.this, "housenumber", "");
        sublocality = baseClass.getSharedPreferance(OrderConfirmationActivity.this, "sublocality", "");
        locality = baseClass.getSharedPreferance(OrderConfirmationActivity.this, "locality", "");
        city = baseClass.getSharedPreferance(OrderConfirmationActivity.this, "city", "");
        total = baseClass.getSharedPreferance(OrderConfirmationActivity.this, "total", "0");

        txt_house.setText(housenumber);
        txt_building.setText(building);
        txt_sublocality.setText(sublocality);
        txt_locality.setText(locality);
        txt_city.setText(city);
        txt_total_price.setText(total);


        baseClass.setSharedPreferance(OrderConfirmationActivity.this, "total", " ");
        baseClass.setSharedPreferance(OrderConfirmationActivity.this, "building", " ");
        baseClass.setSharedPreferance(OrderConfirmationActivity.this, "housenumber", " ");
        baseClass.setSharedPreferance(OrderConfirmationActivity.this, "sublocality", " ");
        baseClass.setSharedPreferance(OrderConfirmationActivity.this, "locality", " ");
        baseClass.setSharedPreferance(OrderConfirmationActivity.this, "city", " ");

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            public void run() {
                Intent intent = new Intent(OrderConfirmationActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        }, 5000);

    }

    // set status bar
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient_toolbar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(OrderConfirmationActivity.this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    public void ThankYouClick(View view) {
        Intent intent = new Intent(OrderConfirmationActivity.this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }
}
