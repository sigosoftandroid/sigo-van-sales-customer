package com.vansale.customer.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vansale.customer.Retrofit.Api;
import com.vansale.customer.Adapter.ProductAdapter;
import com.vansale.customer.BaseClass;
import com.vansale.customer.Model.ProductModel;
import com.vansale.customer.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class ProductActivity extends AppCompatActivity {

    ImageView img_back;
    RecyclerView recyclerView;
    TextView toolbarTitle;
    Button btn_viewcart;

    ProductModel productModel;
    ProductAdapter productAdapter;
    BaseClass baseClass = new BaseClass();
    Typeface typeface,typeface1;

    List<ProductModel> productList = new ArrayList<>();

    String customer_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(ProductActivity.this);
        setContentView(R.layout.activity_product);
        getSupportActionBar().hide();

        img_back = findViewById(R.id.img_back);
        recyclerView = findViewById(R.id.recycler_view);
        toolbarTitle = findViewById(R.id.toolbar_title);
        btn_viewcart = findViewById(R.id.btn_viewcart);

        typeface = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        toolbarTitle.setTypeface(typeface);
        typeface1 = Typeface.createFromAsset(getAssets(), "fonts/Avenir_LTD_Light.ttf");
        btn_viewcart.setTypeface(typeface1);

        customer_id = baseClass.getSharedPreferance(ProductActivity.this,"UserId","0");

        ViewProducts(customer_id);


    }

    //color for status bar
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient_toolbar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    public void ViewProducts(final String customer_id) {
        if(NetworkConnection.isNetworkStatusAvialable(ProductActivity.this)) {
            final ProgressDialog dialog = new ProgressDialog(ProductActivity.this);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
            Api api = BaseClass.getApi();
            Call<ResponseBody> call = api.getProducts(customer_id);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                    if (response.isSuccessful()) {
                        dialog.dismiss();
                        String s = null;
                        try {
                            s = response.body().string();
                            System.out.println("Response.........." + s);
                            JSONObject jo = new JSONObject(s);

                            String message = jo.getString("message");
                            if (message.equals("success")) {
                                productList.clear();

                                JSONArray jsonArray = jo.getJSONArray("products");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    String product_id = jsonObject.getString("product_id");
                                    String category_id = jsonObject.getString("cat_id");
                                    String product_name = jsonObject.getString("product_name");
                                    String product_name_arabic = jsonObject.getString("product_name_arabic");
                                    String product_vat = jsonObject.getString("product_vat");
                                    String price = jsonObject.getString("price");
                                    String image = BaseClass.mainURL + jsonObject.getString("product_image");
                                    String category = jsonObject.getString("category");
                                    String quantity = jsonObject.getString("quantity");
                                    String stock = jsonObject.getString("stock");
                                    String coupons  = jsonObject.getString("coupons");
                                    String timestamp = jsonObject.getString("timestamp");

                                    productModel = new ProductModel(product_id, category_id, product_name, product_name_arabic, product_vat, price, image, category, quantity, stock,coupons,timestamp);
                                    productList.add(productModel);
                                }
                                productAdapter = new ProductAdapter(ProductActivity.this, productList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                recyclerView.setLayoutManager(mLayoutManager);
                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                recyclerView.setAdapter(productAdapter);
                            } else {

                                Toast.makeText(ProductActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                            }


                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {

                        Toast.makeText(ProductActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dialog.dismiss();
                    Toast.makeText(ProductActivity.this, "Connection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }else {
            Toast.makeText(this, "Check your connection", Toast.LENGTH_SHORT).show();
        }


    }



    public void backClick(View view) {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void viewCart(View view) {

        startActivity(new Intent(ProductActivity.this,CartActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        customer_id = baseClass.getSharedPreferance(ProductActivity.this,"UserId","0");
        ViewProducts(customer_id);
    }
}
