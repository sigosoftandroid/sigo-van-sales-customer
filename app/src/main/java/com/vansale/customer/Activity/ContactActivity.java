package com.vansale.customer.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vansale.customer.R;

public class ContactActivity extends AppCompatActivity {

    TextView tv_phone;
    LinearLayout ll_address;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(ContactActivity.this);
        setContentView(R.layout.activity_contact);
        getSupportActionBar().hide();

        tv_phone = findViewById(R.id.txt_phone);
        ll_address = findViewById(R.id.lin_address);

        tv_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:123456789"));
                startActivity(intent);

            }
        });

//        ll_address.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try {
//
//                    double lat = 26.2078084;
//                    double lng = 50.5498006;
//                    String strUri = "http://maps.google.com/maps?q=loc:" + lat + "," + lng + " (" + "NaderGas" + ")";
//                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));
//                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//                    startActivity(intent);
//                }catch (Exception e){
//                    Log.e("ex", String.valueOf(e));
//                }
//
//            }
//        });



    }

    public void backClick(View view) {
        finish();
    }

    // set status bar
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient_toolbar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }
}
