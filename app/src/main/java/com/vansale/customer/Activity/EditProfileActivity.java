package com.vansale.customer.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.vansale.customer.BaseClass;
import com.vansale.customer.R;
import com.vansale.customer.Retrofit.Api;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;
//import com.theartofdev.edmodo.cropper.CropImage;
//import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EditProfileActivity extends AppCompatActivity {

    EditText ed_name_latin, ed_phone, ed_email,ed_address,ed_trn;
    TextView lbl_name_latin,lbl_phone, lbl_email,lbl_address,lbl_trn;
    Button btn_save;
    CircleImageView profile_image;
    String user_id;
    String name_latin, myotp,email, mobile,image;
    String updated_image = "";
    BaseClass baseClass = new BaseClass();
    Typeface typeface;
    int flag_mobile = 0;

    /**
     * Persist URI image to crop URI if specific permissions are required
     */
    private Uri mCropImageUri;
    Bitmap croped_img;
    Bitmap converetdImage;
    String address,trnNumber;
    private Uri cameraImageUri;
    private static final int REQUEST_CAMERA = 1;
    private static final int REQUEST_GALLERY = 2;
    private static final int REQUEST_CROP = 3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(EditProfileActivity.this);
        setContentView(R.layout.activity_edit_profile);
        getSupportActionBar().hide();

        ed_name_latin = findViewById(R.id.ed_name_latin);
        ed_phone = findViewById(R.id.ed_phone);
        ed_email = findViewById(R.id.ed_email);
        ed_address = findViewById(R.id.ed_address);
        ed_trn = findViewById(R.id.ed_trn);

        lbl_name_latin = findViewById(R.id.lbl_name_latin);
        lbl_phone = findViewById(R.id.lbl_phone);
        lbl_email = findViewById(R.id.lbl_email);
        lbl_address = findViewById(R.id.lbl_address);
        lbl_trn = findViewById(R.id.lbl_trn);


        btn_save = findViewById(R.id.btn_save);

        profile_image = findViewById(R.id.iv_edit_profile);

        //setting typeface
        typeface = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");

        ed_name_latin.setTypeface(typeface);
        ed_phone.setTypeface(typeface);
        ed_email.setTypeface(typeface);
        ed_address.setTypeface(typeface);

        lbl_name_latin.setTypeface(typeface);
        lbl_phone.setTypeface(typeface);
        lbl_email.setTypeface(typeface);
        lbl_address.setTypeface(typeface);

        user_id = baseClass.getSharedPreferance(getBaseContext(), "UserId", "0");

        //getting data through intent

        Intent intent = getIntent();
        name_latin = intent.getStringExtra("name_latin");
        email = intent.getStringExtra("email");
        mobile = intent.getStringExtra("mobile");
        image = intent.getStringExtra("image");
        address = intent.getStringExtra("address");
        trnNumber = intent.getStringExtra("trn");


        //setting data in Edittext
        Picasso.with(EditProfileActivity.this).load(image).error(getApplicationContext().getResources().getDrawable(R.drawable.user_default)).into(profile_image);
        ed_name_latin.setText(name_latin);
        ed_address.setText(address);
        ed_phone.setText(mobile);
        ed_email.setText(email);
        ed_trn.setText(trnNumber);


        // Profile image click
        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (ActivityCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.CAMERA)
//                        != PackageManager.PERMISSION_GRANTED) {
//                    // Camera permission has not been granted.
//                    requestCameraPermission();
//
//                } else {
                 openGallery();
//                    onSelectImageClick(view);
//                }

            }
        });

        // Checking for mobile number changes

        ed_phone.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                flag_mobile = 1;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });
    }

    //color for status bar
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient_toolbar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }
    private void requestPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            androidx.core.app.ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, 100);
        }
    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            if (photoFile != null) {
                cameraImageUri = FileProvider.getUriForFile(this, getPackageName() + ".fileprovider", photoFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri);
                startActivityForResult(cameraIntent, REQUEST_CAMERA);
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(null);
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    private void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, REQUEST_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    startCrop(cameraImageUri);
                    break;
                case REQUEST_GALLERY:
                    if (data != null && data.getData() != null) {
                        startCrop(data.getData());
                    }
                    break;
                case UCrop.REQUEST_CROP:
                    if (data != null) {
                        Uri resultUri = UCrop.getOutput(data);
                        if (resultUri != null) {
                            profile_image.setImageURI(resultUri);

                            try {
                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                                croped_img = bitmap;
                                if (croped_img != null) {

                                    converetdImage = getResizedBitmap(bitmap, 200);

                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;
                case UCrop.RESULT_ERROR:
                    Throwable cropError = UCrop.getError(data);
                    if (cropError != null) {
                        Toast.makeText(this, cropError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    break;
            }

        }
    }

    private void startCrop(Uri sourceUri) {
        Uri destinationUri = Uri.fromFile(new File(getCacheDir(), "croppedImage.jpg"));
        UCrop.of(sourceUri, destinationUri)
                .withAspectRatio(1, 1)
                .withMaxResultSize(800, 800)
                .start(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @androidx.annotation.NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permissions granted
            } else {
                Toast.makeText(this, "Permissions denied", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void requestCameraPermission() {
        //   Toast.makeText(EditProfileActivity.this, "CAMERA permission has NOT been granted. Requesting permission.", Toast.LENGTH_SHORT).show();
        if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            //  Toast.makeText(EditProfileActivity.this, "Displaying camera permission rationale to provide additional context.", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(EditProfileActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    13);

        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA},
                    13);

        }
        // END_INCLUDE(camera_permission_request)
    }


//    private void requestCameraPermission() {
//        //   Toast.makeText(EditProfileActivity.this, "CAMERA permission has NOT been granted. Requesting permission.", Toast.LENGTH_SHORT).show();
//        if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
//                Manifest.permission.CAMERA)) {
//            // Provide an additional rationale to the user if the permission was not granted
//            // and the user would benefit from additional context for the use of the permission.
//            // For example if the user has previously denied the permission.
//            //  Toast.makeText(EditProfileActivity.this, "Displaying camera permission rationale to provide additional context.", Toast.LENGTH_SHORT).show();
//            ActivityCompat.requestPermissions(EditProfileActivity.this,
//                    new String[]{Manifest.permission.CAMERA},
//                    13);
//
//        } else {
//
//            // Camera permission has not been granted yet. Request it directly.
//            ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA},
//                    13);
//
//        }
//        // END_INCLUDE(camera_permission_request)
//    }
//
//
//    public void onSelectImageClick(View view) {
//        CropImage.startPickImageActivity(this);
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//
//        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            // required permissions granted, start crop image activity
//            startCropImageActivity(mCropImageUri);
//        } else {
//            // startCropImageActivity(mCropImageUri);
//            CropImage.startPickImageActivity(this);
//            // Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
//        }
//
//    }
//
//    /**
//     * Start crop image activity for the given image.
//     */
//    private void startCropImageActivity(Uri imageUri) {
//        CropImage.activity(imageUri)
//                .setGuidelines(CropImageView.Guidelines.ON)
//                .setMultiTouchEnabled(true)
//                .start(this);
//    }
//
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_CANCELED) {
//            return;
//        }
//
//        // handle result of pick image chooser
//        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
//            Uri imageUri = CropImage.getPickImageResultUri(this, data);
//
//            // For API >= 23 we need to check specifically that we have permissions to read external storage.
//            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
//                // request permissions and handle the result in onRequestPermissionsResult()
//                mCropImageUri = imageUri;
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
//                }
//            } else {
//                // no permissions required or already grunted, can start crop image activity
//                startCropImageActivity(imageUri);
//            }
//        }
//        // handle result of CropImageActivity
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
//                profile_image.setImageURI(result.getUri());
//
//                try {
//                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), result.getUri());
//                    croped_img = bitmap;
//                    if (croped_img != null) {
//
//                        converetdImage = getResizedBitmap(bitmap, 200);
//
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
////                Toast.makeText(this, "Cropping successful, Sample: " + croped_img, Toast.LENGTH_LONG).show();
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
////                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
//            }
//        }
//
//    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public String getStringImage(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public void SaveProfileClick(View view) {
        name_latin = ed_name_latin.getText().toString();
        address = ed_address.getText().toString();
        email = ed_email.getText().toString();
        mobile = ed_phone.getText().toString();
        trnNumber = ed_trn.getText().toString();


        if (name_latin.length() == 0) {

            ed_name_latin.setError(getString(R.string.please_enter_name));
            ed_name_latin.requestFocus();

        }else if(!name_latin.matches("^([A-Za-z]+)(\\s[A-Za-z]+)*\\s?$")){
            Toast.makeText(EditProfileActivity.this, "Name does not accept numbers and special characters", Toast.LENGTH_SHORT).show();
        }
        else if (mobile.length() < 9) {

            ed_phone.setError(getString(R.string.please_enter_valid_phone));
            ed_phone.requestFocus();
        } else if (!isValidEmail(email)) {

            ed_email.setError(getString(R.string.please_enter_valid_email));
            ed_email.requestFocus();

        }else if (address.length() == 0) {

            ed_address.setError(getString(R.string.enteraddress));
            ed_address.requestFocus();

        }
        else

        {
            if (converetdImage != null) {

                updated_image = getStringImage(converetdImage);
            }
            if (flag_mobile == 1) {
                Random rand = new Random();
                myotp = String.format("%04d", rand.nextInt(10000));

//                Toast.makeText(this,myotp, Toast.LENGTH_SHORT).show();
//                ShowDialog();
                sendOTP(mobile,myotp);

            } else {

                SaveChanges();
                Log.e("addree",address);
            }
        }
    }

    private void sendOTP(String phone, final String myotp) {
        final ProgressDialog dialog = new ProgressDialog(EditProfileActivity.this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.show();
        Api api = BaseClass.getApi();
        Call<ResponseBody> call = api.sendOtp(phone,myotp);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    dialog.dismiss();
                    try {
                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                        if (jsonRESULTS.getString("message").equals("Success")) {

                            Toast.makeText(EditProfileActivity.this,"An OTP has been sent to your mobile number successfully", Toast.LENGTH_SHORT).show();
                            Log.e("myotp",myotp);
                            ShowDialog();

                        } else {
                            Toast.makeText(EditProfileActivity.this, "OTP Sending Failed", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                } else {
//                            loading.dismiss();
                    Toast.makeText(EditProfileActivity.this, "Response error", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(EditProfileActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                dialog.dismiss();

            }
        });
    }

    public void SaveChanges() {
        if(NetworkConnection.isNetworkStatusAvialable(EditProfileActivity.this)) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage(getString(R.string.loading_dialog));
            dialog.setCancelable(false);
            dialog.show();

            Api api = BaseClass.getApi();
            Call<ResponseBody> call = api.editCustomer(user_id, name_latin, email, mobile, updated_image, address,trnNumber);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                    if (response.isSuccessful()) {

                        dialog.dismiss();

                        String s = null;
                        try {
                            s = response.body().string();
                            System.out.println("Response.........." + s);
                            JSONObject jo = new JSONObject(s);

                            String status = jo.getString("message");
                            if (status.equals("success")) {

                                Toast.makeText(EditProfileActivity.this, R.string.profile_updated, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(EditProfileActivity.this, MyProfileActivity.class);
                                startActivity(intent);
                                finish();
                            } else if (status.equals("failed")) {

                                String mobileCheck = jo.getString("mobileCheck");
                                String emailCheck = jo.getString("emailCheck");
                                if (mobileCheck.equals("false"))
                                    Toast.makeText(EditProfileActivity.this, R.string.mobile_number_already, Toast.LENGTH_SHORT).show();
                                else if (emailCheck.equals("false"))
                                    Toast.makeText(EditProfileActivity.this, R.string.email_already, Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(EditProfileActivity.this, R.string.try_again_later, Toast.LENGTH_SHORT).show();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {

                        Toast.makeText(EditProfileActivity.this, "Response Error", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dialog.dismiss();
                    Toast.makeText(EditProfileActivity.this, "Connection Failed " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }else {
            Toast.makeText(this, "Check your connection", Toast.LENGTH_SHORT).show();
        }


    }

    // validation for email
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    // validation for name
    boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    public void ShowDialog() {
        final MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .customView(R.layout.dialog_otp, true)
                .cancelable(true)
                .autoDismiss(false);
        dialog = builder.build();
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final EditText editText1 = (EditText) dialog.findViewById(R.id.editext);
        final EditText editText2 = (EditText) dialog.findViewById(R.id.editext1);
        final EditText editText3 = (EditText) dialog.findViewById(R.id.editext2);
        final EditText editText4 = (EditText) dialog.findViewById(R.id.editext3);
        final Button submit = (Button) dialog.findViewById(R.id.btn_otp);

        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (editText1.getText().toString().trim().length() == 1) {
                    editText1.clearFocus();
                    editText2.requestFocus();
                }
            }
        });

        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (editText1.getText().toString().trim().length() == 1) {
                    editText2.clearFocus();
                    editText3.requestFocus();
                }
            }
        });
        editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (editText1.getText().toString().trim().length() == 1) {
                    editText3.clearFocus();
                    editText4.requestFocus();
                }
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String otp1 = editText1.getText().toString();
                String otp2 = editText2.getText().toString();
                String otp3 = editText3.getText().toString();
                String otp4 = editText4.getText().toString();
                String otp = otp1 + otp2 + otp3 + otp4;
                if (myotp.equals(otp)) {
                    SaveChanges();
                } else {
                    Toast.makeText(EditProfileActivity.this, R.string.invalid_otp, Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(EditProfileActivity.this, MyProfileActivity.class));
        finish();
    }

    public void backClick(View view) {
        startActivity(new Intent(EditProfileActivity.this, MyProfileActivity.class));
        finish();
    }
}
